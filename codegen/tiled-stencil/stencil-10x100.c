#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>

#define max(x, y)   ((x)>(y) ? (x) : (y))
#define min(x, y)   ((x)>(y) ? (y) : (x))
#define ceild(n,d)  (int)ceil(((double)(n))/((double)(d)))
#define floord(n,d) (int)floor(((double)(n))/((double)(d)))
#define mallocCheck(v,s,d) if ((v) == NULL) { printf("Failed to allocate memory for %s : size=%lu\n", "sizeof(d)*(s)", sizeof(d)*(s)); exit(-1); }

#define X(i) X[i]
#define Y(t,i) Y[((t))*(N+1)+(i)]
#define C1(tt,ti) C1[(N/100)*(tt)+(ti)]
#define C2(tt,ti) C2[(N/100)*(tt)+(ti)]
#define W(i) weights[i]

#define THRESHOLD 1e-6
#define I(tt,ti) fabs((C2(tt,ti)-C1(tt,ti))/C2(tt,ti)) > THRESHOLD // condition when tile computation is correct

#define S0(tt,ti,t,i) Y(t,i) = X(i)
#define S1(tt,ti,t,i) Y(t,i) = Y(t-1,i)
#define S2(tt,ti,t,i) Y(t,i) = 0.3332*Y(t-1,i-1) + 0.3333*Y(t-1,i) + 0.3334*Y(t-1,i+1)
#define K1(tt,ti,t,i) C1(tt,ti) += Y(t,i)
#define K2(tt,ti,t,i) C2(tt,ti) += W((i)-1-100*(ti)+10) * Y(t,i)
#define TRAPEZOID(tt,ti) do { compute_trapezoid(tt, ti, X, Y, C1, C2, weights, T, N); } while(I(tt,ti))

void compute_trapezoid(int tt, int ti, float* X, float* Y, float* C1, float* C2, float* weights, int T, int N)
{
  S2(tt, ti, 10 * tt, 100 * ti - 9);
  K2(tt, ti, 10 * tt, 100 * ti - 9);
  S2(tt, ti, 10 * tt, 100 * ti - 8);
  K2(tt, ti, 10 * tt, 100 * ti - 8);
  S2(tt, ti, 10 * tt, 100 * ti - 7);
  K2(tt, ti, 10 * tt, 100 * ti - 7);
  S2(tt, ti, 10 * tt, 100 * ti - 6);
  K2(tt, ti, 10 * tt, 100 * ti - 6);
  S2(tt, ti, 10 * tt, 100 * ti - 5);
  K2(tt, ti, 10 * tt, 100 * ti - 5);
  S2(tt, ti, 10 * tt, 100 * ti - 4);
  K2(tt, ti, 10 * tt, 100 * ti - 4);
  S2(tt, ti, 10 * tt, 100 * ti - 3);
  K2(tt, ti, 10 * tt, 100 * ti - 3);
  S2(tt, ti, 10 * tt, 100 * ti - 2);
  K2(tt, ti, 10 * tt, 100 * ti - 2);
  S2(tt, ti, 10 * tt, 100 * ti - 1);
  K2(tt, ti, 10 * tt, 100 * ti - 1);
  S2(tt, ti, 10 * tt, 100 * ti);
  K2(tt, ti, 10 * tt, 100 * ti);
  S2(tt, ti, 10 * tt, 100 * ti + 1);
  K2(tt, ti, 10 * tt, 100 * ti + 1);
  S2(tt, ti, 10 * tt, 100 * ti + 2);
  K2(tt, ti, 10 * tt, 100 * ti + 2);
  S2(tt, ti, 10 * tt, 100 * ti + 3);
  K2(tt, ti, 10 * tt, 100 * ti + 3);
  S2(tt, ti, 10 * tt, 100 * ti + 4);
  K2(tt, ti, 10 * tt, 100 * ti + 4);
  S2(tt, ti, 10 * tt, 100 * ti + 5);
  K2(tt, ti, 10 * tt, 100 * ti + 5);
  S2(tt, ti, 10 * tt, 100 * ti + 6);
  K2(tt, ti, 10 * tt, 100 * ti + 6);
  S2(tt, ti, 10 * tt, 100 * ti + 7);
  K2(tt, ti, 10 * tt, 100 * ti + 7);
  S2(tt, ti, 10 * tt, 100 * ti + 8);
  K2(tt, ti, 10 * tt, 100 * ti + 8);
  S2(tt, ti, 10 * tt, 100 * ti + 9);
  K2(tt, ti, 10 * tt, 100 * ti + 9);
  S2(tt, ti, 10 * tt, 100 * ti + 10);
  K2(tt, ti, 10 * tt, 100 * ti + 10);
  S2(tt, ti, 10 * tt, 100 * ti + 11);
  K2(tt, ti, 10 * tt, 100 * ti + 11);
  S2(tt, ti, 10 * tt, 100 * ti + 12);
  K2(tt, ti, 10 * tt, 100 * ti + 12);
  S2(tt, ti, 10 * tt, 100 * ti + 13);
  K2(tt, ti, 10 * tt, 100 * ti + 13);
  S2(tt, ti, 10 * tt, 100 * ti + 14);
  K2(tt, ti, 10 * tt, 100 * ti + 14);
  S2(tt, ti, 10 * tt, 100 * ti + 15);
  K2(tt, ti, 10 * tt, 100 * ti + 15);
  S2(tt, ti, 10 * tt, 100 * ti + 16);
  K2(tt, ti, 10 * tt, 100 * ti + 16);
  S2(tt, ti, 10 * tt, 100 * ti + 17);
  K2(tt, ti, 10 * tt, 100 * ti + 17);
  S2(tt, ti, 10 * tt, 100 * ti + 18);
  K2(tt, ti, 10 * tt, 100 * ti + 18);
  S2(tt, ti, 10 * tt, 100 * ti + 19);
  K2(tt, ti, 10 * tt, 100 * ti + 19);
  S2(tt, ti, 10 * tt, 100 * ti + 20);
  K2(tt, ti, 10 * tt, 100 * ti + 20);
  S2(tt, ti, 10 * tt, 100 * ti + 21);
  K2(tt, ti, 10 * tt, 100 * ti + 21);
  S2(tt, ti, 10 * tt, 100 * ti + 22);
  K2(tt, ti, 10 * tt, 100 * ti + 22);
  S2(tt, ti, 10 * tt, 100 * ti + 23);
  K2(tt, ti, 10 * tt, 100 * ti + 23);
  S2(tt, ti, 10 * tt, 100 * ti + 24);
  K2(tt, ti, 10 * tt, 100 * ti + 24);
  S2(tt, ti, 10 * tt, 100 * ti + 25);
  K2(tt, ti, 10 * tt, 100 * ti + 25);
  S2(tt, ti, 10 * tt, 100 * ti + 26);
  K2(tt, ti, 10 * tt, 100 * ti + 26);
  S2(tt, ti, 10 * tt, 100 * ti + 27);
  K2(tt, ti, 10 * tt, 100 * ti + 27);
  S2(tt, ti, 10 * tt, 100 * ti + 28);
  K2(tt, ti, 10 * tt, 100 * ti + 28);
  S2(tt, ti, 10 * tt, 100 * ti + 29);
  K2(tt, ti, 10 * tt, 100 * ti + 29);
  S2(tt, ti, 10 * tt, 100 * ti + 30);
  K2(tt, ti, 10 * tt, 100 * ti + 30);
  S2(tt, ti, 10 * tt, 100 * ti + 31);
  K2(tt, ti, 10 * tt, 100 * ti + 31);
  S2(tt, ti, 10 * tt, 100 * ti + 32);
  K2(tt, ti, 10 * tt, 100 * ti + 32);
  S2(tt, ti, 10 * tt, 100 * ti + 33);
  K2(tt, ti, 10 * tt, 100 * ti + 33);
  S2(tt, ti, 10 * tt, 100 * ti + 34);
  K2(tt, ti, 10 * tt, 100 * ti + 34);
  S2(tt, ti, 10 * tt, 100 * ti + 35);
  K2(tt, ti, 10 * tt, 100 * ti + 35);
  S2(tt, ti, 10 * tt, 100 * ti + 36);
  K2(tt, ti, 10 * tt, 100 * ti + 36);
  S2(tt, ti, 10 * tt, 100 * ti + 37);
  K2(tt, ti, 10 * tt, 100 * ti + 37);
  S2(tt, ti, 10 * tt, 100 * ti + 38);
  K2(tt, ti, 10 * tt, 100 * ti + 38);
  S2(tt, ti, 10 * tt, 100 * ti + 39);
  K2(tt, ti, 10 * tt, 100 * ti + 39);
  S2(tt, ti, 10 * tt, 100 * ti + 40);
  K2(tt, ti, 10 * tt, 100 * ti + 40);
  S2(tt, ti, 10 * tt, 100 * ti + 41);
  K2(tt, ti, 10 * tt, 100 * ti + 41);
  S2(tt, ti, 10 * tt, 100 * ti + 42);
  K2(tt, ti, 10 * tt, 100 * ti + 42);
  S2(tt, ti, 10 * tt, 100 * ti + 43);
  K2(tt, ti, 10 * tt, 100 * ti + 43);
  S2(tt, ti, 10 * tt, 100 * ti + 44);
  K2(tt, ti, 10 * tt, 100 * ti + 44);
  S2(tt, ti, 10 * tt, 100 * ti + 45);
  K2(tt, ti, 10 * tt, 100 * ti + 45);
  S2(tt, ti, 10 * tt, 100 * ti + 46);
  K2(tt, ti, 10 * tt, 100 * ti + 46);
  S2(tt, ti, 10 * tt, 100 * ti + 47);
  K2(tt, ti, 10 * tt, 100 * ti + 47);
  S2(tt, ti, 10 * tt, 100 * ti + 48);
  K2(tt, ti, 10 * tt, 100 * ti + 48);
  S2(tt, ti, 10 * tt, 100 * ti + 49);
  K2(tt, ti, 10 * tt, 100 * ti + 49);
  S2(tt, ti, 10 * tt, 100 * ti + 50);
  K2(tt, ti, 10 * tt, 100 * ti + 50);
  S2(tt, ti, 10 * tt, 100 * ti + 51);
  K2(tt, ti, 10 * tt, 100 * ti + 51);
  S2(tt, ti, 10 * tt, 100 * ti + 52);
  K2(tt, ti, 10 * tt, 100 * ti + 52);
  S2(tt, ti, 10 * tt, 100 * ti + 53);
  K2(tt, ti, 10 * tt, 100 * ti + 53);
  S2(tt, ti, 10 * tt, 100 * ti + 54);
  K2(tt, ti, 10 * tt, 100 * ti + 54);
  S2(tt, ti, 10 * tt, 100 * ti + 55);
  K2(tt, ti, 10 * tt, 100 * ti + 55);
  S2(tt, ti, 10 * tt, 100 * ti + 56);
  K2(tt, ti, 10 * tt, 100 * ti + 56);
  S2(tt, ti, 10 * tt, 100 * ti + 57);
  K2(tt, ti, 10 * tt, 100 * ti + 57);
  S2(tt, ti, 10 * tt, 100 * ti + 58);
  K2(tt, ti, 10 * tt, 100 * ti + 58);
  S2(tt, ti, 10 * tt, 100 * ti + 59);
  K2(tt, ti, 10 * tt, 100 * ti + 59);
  S2(tt, ti, 10 * tt, 100 * ti + 60);
  K2(tt, ti, 10 * tt, 100 * ti + 60);
  S2(tt, ti, 10 * tt, 100 * ti + 61);
  K2(tt, ti, 10 * tt, 100 * ti + 61);
  S2(tt, ti, 10 * tt, 100 * ti + 62);
  K2(tt, ti, 10 * tt, 100 * ti + 62);
  S2(tt, ti, 10 * tt, 100 * ti + 63);
  K2(tt, ti, 10 * tt, 100 * ti + 63);
  S2(tt, ti, 10 * tt, 100 * ti + 64);
  K2(tt, ti, 10 * tt, 100 * ti + 64);
  S2(tt, ti, 10 * tt, 100 * ti + 65);
  K2(tt, ti, 10 * tt, 100 * ti + 65);
  S2(tt, ti, 10 * tt, 100 * ti + 66);
  K2(tt, ti, 10 * tt, 100 * ti + 66);
  S2(tt, ti, 10 * tt, 100 * ti + 67);
  K2(tt, ti, 10 * tt, 100 * ti + 67);
  S2(tt, ti, 10 * tt, 100 * ti + 68);
  K2(tt, ti, 10 * tt, 100 * ti + 68);
  S2(tt, ti, 10 * tt, 100 * ti + 69);
  K2(tt, ti, 10 * tt, 100 * ti + 69);
  S2(tt, ti, 10 * tt, 100 * ti + 70);
  K2(tt, ti, 10 * tt, 100 * ti + 70);
  S2(tt, ti, 10 * tt, 100 * ti + 71);
  K2(tt, ti, 10 * tt, 100 * ti + 71);
  S2(tt, ti, 10 * tt, 100 * ti + 72);
  K2(tt, ti, 10 * tt, 100 * ti + 72);
  S2(tt, ti, 10 * tt, 100 * ti + 73);
  K2(tt, ti, 10 * tt, 100 * ti + 73);
  S2(tt, ti, 10 * tt, 100 * ti + 74);
  K2(tt, ti, 10 * tt, 100 * ti + 74);
  S2(tt, ti, 10 * tt, 100 * ti + 75);
  K2(tt, ti, 10 * tt, 100 * ti + 75);
  S2(tt, ti, 10 * tt, 100 * ti + 76);
  K2(tt, ti, 10 * tt, 100 * ti + 76);
  S2(tt, ti, 10 * tt, 100 * ti + 77);
  K2(tt, ti, 10 * tt, 100 * ti + 77);
  S2(tt, ti, 10 * tt, 100 * ti + 78);
  K2(tt, ti, 10 * tt, 100 * ti + 78);
  S2(tt, ti, 10 * tt, 100 * ti + 79);
  K2(tt, ti, 10 * tt, 100 * ti + 79);
  S2(tt, ti, 10 * tt, 100 * ti + 80);
  K2(tt, ti, 10 * tt, 100 * ti + 80);
  S2(tt, ti, 10 * tt, 100 * ti + 81);
  K2(tt, ti, 10 * tt, 100 * ti + 81);
  S2(tt, ti, 10 * tt, 100 * ti + 82);
  K2(tt, ti, 10 * tt, 100 * ti + 82);
  S2(tt, ti, 10 * tt, 100 * ti + 83);
  K2(tt, ti, 10 * tt, 100 * ti + 83);
  S2(tt, ti, 10 * tt, 100 * ti + 84);
  K2(tt, ti, 10 * tt, 100 * ti + 84);
  S2(tt, ti, 10 * tt, 100 * ti + 85);
  K2(tt, ti, 10 * tt, 100 * ti + 85);
  S2(tt, ti, 10 * tt, 100 * ti + 86);
  K2(tt, ti, 10 * tt, 100 * ti + 86);
  S2(tt, ti, 10 * tt, 100 * ti + 87);
  K2(tt, ti, 10 * tt, 100 * ti + 87);
  S2(tt, ti, 10 * tt, 100 * ti + 88);
  K2(tt, ti, 10 * tt, 100 * ti + 88);
  S2(tt, ti, 10 * tt, 100 * ti + 89);
  K2(tt, ti, 10 * tt, 100 * ti + 89);
  S2(tt, ti, 10 * tt, 100 * ti + 90);
  K2(tt, ti, 10 * tt, 100 * ti + 90);
  S2(tt, ti, 10 * tt, 100 * ti + 91);
  K2(tt, ti, 10 * tt, 100 * ti + 91);
  S2(tt, ti, 10 * tt, 100 * ti + 92);
  K2(tt, ti, 10 * tt, 100 * ti + 92);
  S2(tt, ti, 10 * tt, 100 * ti + 93);
  K2(tt, ti, 10 * tt, 100 * ti + 93);
  S2(tt, ti, 10 * tt, 100 * ti + 94);
  K2(tt, ti, 10 * tt, 100 * ti + 94);
  S2(tt, ti, 10 * tt, 100 * ti + 95);
  K2(tt, ti, 10 * tt, 100 * ti + 95);
  S2(tt, ti, 10 * tt, 100 * ti + 96);
  K2(tt, ti, 10 * tt, 100 * ti + 96);
  S2(tt, ti, 10 * tt, 100 * ti + 97);
  K2(tt, ti, 10 * tt, 100 * ti + 97);
  S2(tt, ti, 10 * tt, 100 * ti + 98);
  K2(tt, ti, 10 * tt, 100 * ti + 98);
  S2(tt, ti, 10 * tt, 100 * ti + 99);
  K2(tt, ti, 10 * tt, 100 * ti + 99);
  S2(tt, ti, 10 * tt, 100 * ti + 100);
  K2(tt, ti, 10 * tt, 100 * ti + 100);
  S2(tt, ti, 10 * tt, 100 * ti + 101);
  K2(tt, ti, 10 * tt, 100 * ti + 101);
  S2(tt, ti, 10 * tt, 100 * ti + 102);
  K2(tt, ti, 10 * tt, 100 * ti + 102);
  S2(tt, ti, 10 * tt, 100 * ti + 103);
  K2(tt, ti, 10 * tt, 100 * ti + 103);
  S2(tt, ti, 10 * tt, 100 * ti + 104);
  K2(tt, ti, 10 * tt, 100 * ti + 104);
  S2(tt, ti, 10 * tt, 100 * ti + 105);
  K2(tt, ti, 10 * tt, 100 * ti + 105);
  S2(tt, ti, 10 * tt, 100 * ti + 106);
  K2(tt, ti, 10 * tt, 100 * ti + 106);
  S2(tt, ti, 10 * tt, 100 * ti + 107);
  K2(tt, ti, 10 * tt, 100 * ti + 107);
  S2(tt, ti, 10 * tt, 100 * ti + 108);
  K2(tt, ti, 10 * tt, 100 * ti + 108);
  S2(tt, ti, 10 * tt + 1, 100 * ti - 8);
  S2(tt, ti, 10 * tt + 1, 100 * ti - 7);
  S2(tt, ti, 10 * tt + 1, 100 * ti - 6);
  S2(tt, ti, 10 * tt + 1, 100 * ti - 5);
  S2(tt, ti, 10 * tt + 1, 100 * ti - 4);
  S2(tt, ti, 10 * tt + 1, 100 * ti - 3);
  S2(tt, ti, 10 * tt + 1, 100 * ti - 2);
  S2(tt, ti, 10 * tt + 1, 100 * ti - 1);
  S2(tt, ti, 10 * tt + 1, 100 * ti);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 1);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 2);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 3);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 4);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 5);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 6);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 7);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 8);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 9);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 10);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 11);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 12);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 13);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 14);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 15);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 16);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 17);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 18);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 19);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 20);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 21);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 22);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 23);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 24);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 25);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 26);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 27);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 28);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 29);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 30);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 31);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 32);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 33);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 34);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 35);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 36);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 37);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 38);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 39);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 40);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 41);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 42);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 43);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 44);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 45);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 46);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 47);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 48);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 49);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 50);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 51);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 52);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 53);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 54);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 55);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 56);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 57);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 58);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 59);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 60);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 61);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 62);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 63);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 64);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 65);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 66);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 67);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 68);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 69);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 70);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 71);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 72);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 73);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 74);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 75);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 76);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 77);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 78);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 79);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 80);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 81);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 82);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 83);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 84);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 85);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 86);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 87);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 88);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 89);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 90);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 91);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 92);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 93);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 94);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 95);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 96);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 97);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 98);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 99);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 100);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 101);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 102);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 103);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 104);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 105);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 106);
  S2(tt, ti, 10 * tt + 1, 100 * ti + 107);
  S2(tt, ti, 10 * tt + 2, 100 * ti - 7);
  S2(tt, ti, 10 * tt + 2, 100 * ti - 6);
  S2(tt, ti, 10 * tt + 2, 100 * ti - 5);
  S2(tt, ti, 10 * tt + 2, 100 * ti - 4);
  S2(tt, ti, 10 * tt + 2, 100 * ti - 3);
  S2(tt, ti, 10 * tt + 2, 100 * ti - 2);
  S2(tt, ti, 10 * tt + 2, 100 * ti - 1);
  S2(tt, ti, 10 * tt + 2, 100 * ti);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 1);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 2);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 3);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 4);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 5);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 6);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 7);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 8);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 9);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 10);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 11);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 12);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 13);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 14);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 15);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 16);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 17);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 18);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 19);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 20);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 21);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 22);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 23);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 24);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 25);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 26);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 27);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 28);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 29);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 30);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 31);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 32);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 33);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 34);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 35);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 36);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 37);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 38);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 39);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 40);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 41);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 42);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 43);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 44);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 45);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 46);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 47);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 48);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 49);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 50);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 51);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 52);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 53);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 54);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 55);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 56);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 57);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 58);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 59);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 60);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 61);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 62);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 63);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 64);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 65);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 66);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 67);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 68);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 69);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 70);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 71);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 72);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 73);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 74);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 75);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 76);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 77);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 78);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 79);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 80);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 81);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 82);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 83);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 84);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 85);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 86);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 87);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 88);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 89);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 90);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 91);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 92);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 93);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 94);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 95);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 96);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 97);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 98);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 99);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 100);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 101);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 102);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 103);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 104);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 105);
  S2(tt, ti, 10 * tt + 2, 100 * ti + 106);
  S2(tt, ti, 10 * tt + 3, 100 * ti - 6);
  S2(tt, ti, 10 * tt + 3, 100 * ti - 5);
  S2(tt, ti, 10 * tt + 3, 100 * ti - 4);
  S2(tt, ti, 10 * tt + 3, 100 * ti - 3);
  S2(tt, ti, 10 * tt + 3, 100 * ti - 2);
  S2(tt, ti, 10 * tt + 3, 100 * ti - 1);
  S2(tt, ti, 10 * tt + 3, 100 * ti);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 1);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 2);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 3);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 4);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 5);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 6);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 7);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 8);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 9);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 10);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 11);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 12);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 13);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 14);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 15);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 16);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 17);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 18);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 19);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 20);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 21);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 22);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 23);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 24);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 25);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 26);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 27);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 28);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 29);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 30);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 31);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 32);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 33);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 34);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 35);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 36);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 37);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 38);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 39);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 40);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 41);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 42);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 43);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 44);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 45);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 46);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 47);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 48);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 49);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 50);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 51);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 52);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 53);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 54);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 55);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 56);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 57);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 58);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 59);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 60);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 61);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 62);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 63);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 64);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 65);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 66);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 67);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 68);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 69);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 70);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 71);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 72);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 73);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 74);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 75);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 76);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 77);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 78);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 79);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 80);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 81);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 82);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 83);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 84);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 85);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 86);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 87);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 88);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 89);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 90);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 91);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 92);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 93);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 94);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 95);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 96);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 97);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 98);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 99);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 100);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 101);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 102);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 103);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 104);
  S2(tt, ti, 10 * tt + 3, 100 * ti + 105);
  S2(tt, ti, 10 * tt + 4, 100 * ti - 5);
  S2(tt, ti, 10 * tt + 4, 100 * ti - 4);
  S2(tt, ti, 10 * tt + 4, 100 * ti - 3);
  S2(tt, ti, 10 * tt + 4, 100 * ti - 2);
  S2(tt, ti, 10 * tt + 4, 100 * ti - 1);
  S2(tt, ti, 10 * tt + 4, 100 * ti);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 1);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 2);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 3);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 4);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 5);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 6);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 7);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 8);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 9);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 10);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 11);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 12);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 13);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 14);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 15);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 16);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 17);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 18);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 19);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 20);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 21);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 22);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 23);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 24);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 25);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 26);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 27);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 28);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 29);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 30);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 31);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 32);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 33);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 34);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 35);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 36);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 37);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 38);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 39);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 40);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 41);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 42);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 43);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 44);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 45);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 46);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 47);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 48);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 49);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 50);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 51);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 52);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 53);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 54);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 55);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 56);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 57);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 58);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 59);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 60);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 61);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 62);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 63);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 64);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 65);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 66);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 67);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 68);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 69);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 70);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 71);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 72);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 73);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 74);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 75);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 76);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 77);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 78);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 79);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 80);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 81);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 82);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 83);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 84);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 85);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 86);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 87);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 88);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 89);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 90);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 91);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 92);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 93);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 94);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 95);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 96);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 97);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 98);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 99);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 100);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 101);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 102);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 103);
  S2(tt, ti, 10 * tt + 4, 100 * ti + 104);
  S2(tt, ti, 10 * tt + 5, 100 * ti - 4);
  S2(tt, ti, 10 * tt + 5, 100 * ti - 3);
  S2(tt, ti, 10 * tt + 5, 100 * ti - 2);
  S2(tt, ti, 10 * tt + 5, 100 * ti - 1);
  S2(tt, ti, 10 * tt + 5, 100 * ti);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 1);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 2);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 3);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 4);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 5);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 6);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 7);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 8);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 9);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 10);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 11);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 12);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 13);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 14);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 15);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 16);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 17);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 18);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 19);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 20);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 21);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 22);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 23);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 24);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 25);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 26);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 27);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 28);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 29);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 30);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 31);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 32);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 33);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 34);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 35);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 36);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 37);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 38);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 39);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 40);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 41);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 42);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 43);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 44);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 45);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 46);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 47);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 48);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 49);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 50);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 51);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 52);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 53);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 54);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 55);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 56);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 57);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 58);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 59);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 60);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 61);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 62);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 63);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 64);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 65);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 66);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 67);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 68);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 69);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 70);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 71);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 72);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 73);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 74);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 75);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 76);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 77);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 78);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 79);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 80);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 81);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 82);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 83);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 84);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 85);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 86);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 87);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 88);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 89);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 90);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 91);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 92);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 93);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 94);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 95);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 96);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 97);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 98);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 99);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 100);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 101);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 102);
  S2(tt, ti, 10 * tt + 5, 100 * ti + 103);
  S2(tt, ti, 10 * tt + 6, 100 * ti - 3);
  S2(tt, ti, 10 * tt + 6, 100 * ti - 2);
  S2(tt, ti, 10 * tt + 6, 100 * ti - 1);
  S2(tt, ti, 10 * tt + 6, 100 * ti);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 1);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 2);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 3);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 4);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 5);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 6);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 7);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 8);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 9);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 10);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 11);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 12);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 13);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 14);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 15);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 16);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 17);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 18);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 19);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 20);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 21);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 22);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 23);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 24);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 25);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 26);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 27);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 28);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 29);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 30);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 31);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 32);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 33);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 34);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 35);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 36);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 37);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 38);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 39);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 40);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 41);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 42);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 43);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 44);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 45);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 46);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 47);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 48);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 49);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 50);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 51);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 52);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 53);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 54);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 55);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 56);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 57);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 58);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 59);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 60);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 61);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 62);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 63);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 64);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 65);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 66);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 67);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 68);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 69);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 70);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 71);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 72);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 73);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 74);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 75);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 76);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 77);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 78);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 79);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 80);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 81);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 82);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 83);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 84);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 85);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 86);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 87);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 88);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 89);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 90);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 91);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 92);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 93);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 94);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 95);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 96);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 97);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 98);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 99);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 100);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 101);
  S2(tt, ti, 10 * tt + 6, 100 * ti + 102);
  S2(tt, ti, 10 * tt + 7, 100 * ti - 2);
  S2(tt, ti, 10 * tt + 7, 100 * ti - 1);
  S2(tt, ti, 10 * tt + 7, 100 * ti);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 1);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 2);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 3);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 4);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 5);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 6);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 7);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 8);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 9);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 10);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 11);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 12);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 13);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 14);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 15);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 16);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 17);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 18);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 19);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 20);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 21);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 22);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 23);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 24);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 25);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 26);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 27);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 28);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 29);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 30);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 31);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 32);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 33);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 34);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 35);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 36);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 37);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 38);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 39);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 40);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 41);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 42);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 43);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 44);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 45);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 46);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 47);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 48);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 49);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 50);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 51);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 52);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 53);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 54);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 55);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 56);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 57);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 58);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 59);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 60);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 61);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 62);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 63);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 64);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 65);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 66);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 67);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 68);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 69);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 70);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 71);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 72);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 73);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 74);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 75);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 76);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 77);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 78);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 79);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 80);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 81);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 82);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 83);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 84);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 85);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 86);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 87);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 88);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 89);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 90);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 91);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 92);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 93);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 94);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 95);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 96);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 97);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 98);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 99);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 100);
  S2(tt, ti, 10 * tt + 7, 100 * ti + 101);
  S2(tt, ti, 10 * tt + 8, 100 * ti - 1);
  S2(tt, ti, 10 * tt + 8, 100 * ti);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 1);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 2);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 3);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 4);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 5);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 6);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 7);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 8);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 9);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 10);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 11);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 12);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 13);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 14);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 15);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 16);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 17);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 18);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 19);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 20);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 21);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 22);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 23);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 24);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 25);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 26);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 27);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 28);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 29);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 30);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 31);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 32);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 33);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 34);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 35);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 36);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 37);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 38);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 39);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 40);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 41);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 42);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 43);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 44);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 45);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 46);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 47);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 48);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 49);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 50);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 51);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 52);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 53);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 54);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 55);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 56);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 57);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 58);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 59);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 60);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 61);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 62);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 63);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 64);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 65);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 66);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 67);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 68);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 69);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 70);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 71);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 72);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 73);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 74);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 75);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 76);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 77);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 78);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 79);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 80);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 81);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 82);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 83);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 84);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 85);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 86);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 87);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 88);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 89);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 90);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 91);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 92);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 93);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 94);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 95);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 96);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 97);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 98);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 99);
  S2(tt, ti, 10 * tt + 8, 100 * ti + 100);
  S2(tt, ti, 10 * tt + 9, 100 * ti);
  K1(tt, ti, 10 * tt + 9, 100 * ti);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 1);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 1);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 2);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 2);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 3);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 3);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 4);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 4);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 5);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 5);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 6);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 6);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 7);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 7);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 8);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 8);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 9);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 9);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 10);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 10);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 11);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 11);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 12);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 12);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 13);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 13);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 14);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 14);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 15);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 15);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 16);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 16);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 17);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 17);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 18);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 18);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 19);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 19);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 20);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 20);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 21);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 21);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 22);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 22);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 23);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 23);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 24);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 24);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 25);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 25);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 26);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 26);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 27);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 27);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 28);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 28);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 29);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 29);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 30);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 30);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 31);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 31);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 32);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 32);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 33);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 33);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 34);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 34);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 35);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 35);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 36);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 36);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 37);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 37);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 38);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 38);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 39);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 39);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 40);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 40);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 41);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 41);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 42);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 42);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 43);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 43);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 44);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 44);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 45);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 45);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 46);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 46);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 47);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 47);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 48);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 48);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 49);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 49);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 50);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 50);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 51);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 51);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 52);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 52);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 53);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 53);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 54);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 54);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 55);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 55);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 56);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 56);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 57);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 57);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 58);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 58);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 59);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 59);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 60);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 60);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 61);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 61);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 62);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 62);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 63);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 63);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 64);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 64);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 65);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 65);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 66);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 66);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 67);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 67);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 68);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 68);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 69);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 69);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 70);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 70);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 71);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 71);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 72);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 72);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 73);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 73);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 74);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 74);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 75);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 75);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 76);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 76);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 77);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 77);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 78);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 78);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 79);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 79);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 80);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 80);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 81);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 81);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 82);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 82);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 83);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 83);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 84);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 84);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 85);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 85);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 86);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 86);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 87);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 87);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 88);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 88);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 89);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 89);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 90);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 90);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 91);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 91);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 92);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 92);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 93);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 93);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 94);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 94);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 95);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 95);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 96);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 96);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 97);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 97);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 98);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 98);
  S2(tt, ti, 10 * tt + 9, 100 * ti + 99);
  K1(tt, ti, 10 * tt + 9, 100 * ti + 99);
}

void stencil_abft(float* X, float* Y, float* C1, float* C2, float* weights, int T, int N)
{
  /*
   * domain: "[T, N] -> { S1[tt, ti, t, i = N] : T >= 41 and N >= 101 and 100ti <= N and t >= 10tt and 2 <= t <= 108 - N + 10tt + 100ti and t <= 9 + 10tt and t <= T; S1[tt, ti = 0, t, i = 0] : T >= 41 and N >= 101 and t >= 10tt and 2 <= t <= 9 + 10tt and t <= T; S0[tt = 0, ti, t, i] : T >= 41 and N >= 101 and ti >= 0 and 100ti <= N and 0 <= t <= 1 and i >= -9 + 100ti + t and 0 <= i <= 108 + 100ti - t and i <= N; K2[tt, ti, t = 10tt, i] : T >= 41 and tt > 0 and 10tt <= -10 + T and ti > 0 and 100ti <= -110 + N and -9 + 100ti <= i <= 108 + 100ti and i < N; S2[tt, ti, t, i] : T >= 41 and N >= 101 and ti >= 0 and 100ti <= N and t >= 10tt and 2 <= t <= 9 + 10tt and t <= T and i > 0 and -9 - 10tt + 100ti + t <= i <= 108 + 10tt + 100ti - t and i < N; K1[tt, ti, t = 9 + 10tt, i] : T >= 41 and tt > 0 and 10tt <= -10 + T and ti > 0 and 100ti <= -110 + N and 100ti <= i <= 99 + 100ti and i < N }"
   * child:
   *   sequence:
   *   - filter: "{ S0[tt, ti, t, i] }"
   *   - filter: "{ S2[tt, ti, t, i]; S1[tt, ti, t, i]; K2[tt, ti, t, i]; K1[tt, ti, t, i] }"
   *     child:
   *       schedule: "[T, N] -> [{ S1[tt, ti, t, i] -> [(tt)]; K2[tt, ti, t, i] -> [(tt)]; S2[tt, ti, t, i] -> [(tt)]; K1[tt, ti, t, i] -> [(tt)] }, { S1[tt, ti, t, i] -> [(ti)]; K2[tt, ti, t, i] -> [(ti)]; S2[tt, ti, t, i] -> [(ti)]; K1[tt, ti, t, i] -> [(ti)] }, { S1[tt, ti, t, i] -> [(t)]; K2[tt, ti, t, i] -> [(t)]; S2[tt, ti, t, i] -> [(t)]; K1[tt, ti, t, i] -> [(t)] }, { S1[tt, ti, t, i] -> [(i)]; K2[tt, ti, t, i] -> [(i)]; S2[tt, ti, t, i] -> [(i)]; K1[tt, ti, t, i] -> [(i)] }]"
   *       options: "[T, N] -> { isolate[[] -> [tt, ti, t, i]] : tt > 0 and 10tt <= -10 + T and ti > 0 and 100ti <= -110 + N; [isolate[] -> unroll[i0]] : 2 <= i0 <= 3 }"
   *       child:
   *         sequence:
   *         - filter: "{ S2[tt, ti, t, i]; S1[tt, ti, t, i] }"
   *         - filter: "{ K2[tt, ti, t, i]; K1[tt, ti, t, i] }"
   */
  for (int ti = 0; ti <= N / 100; ti += 1) {
    for (int t = 0; t <= 1; t += 1) {
      for (int i = max(0, 100 * ti + t - 9); i <= min(N, 100 * ti - t + 108); i += 1) {
        S0(0, ti, t, i);
      }
    }
  }
  if (N >= 210) {
    for (int ti = 0; ti <= N / 100; ti += 1) {
      for (int t = 2; t <= 9; t += 1) {
        for (int i = max(0, 100 * ti + t - 9); i <= min(N, 100 * ti - t + 108); i += 1) {
          if (i >= 1 && N >= i + 1) {
            S2(0, ti, t, i);
          } else {
            S1(0, ti, t, i);
          }
        }
      }
    }
    for (int tt = 1; tt < T / 10; tt += 1) {
      for (int t = 10 * tt; t <= 10 * tt + 9; t += 1) {
        S1(tt, 0, t, 0);
        for (int i = 1; i <= 10 * tt - t + 108; i += 1) {
          S2(tt, 0, t, i);
        }
      }
      for (int ti = 1; ti < (N - 10) / 100; ti += 1) {
        TRAPEZOID(tt, ti);
      }
      for (int ti = (N - 10) / 100; ti <= N / 100; ti += 1) {
        for (int t = 10 * tt; t <= 10 * tt + 9; t += 1) {
          for (int i = -10 * tt + 100 * ti + t - 9; i <= min(N - 1, 10 * tt + 100 * ti - t + 108); i += 1) {
            S2(tt, ti, t, i);
          }
          if (10 * tt + 100 * ti + 108 >= N + t) {
            S1(tt, ti, t, N);
          }
        }
      }
    }
    for (int ti = 0; ti <= N / 100; ti += 1) {
      for (int t = -(T % 10) + T; t <= T; t += 1) {
        for (int i = max(0, (T % 10) - T + 100 * ti + t - 9); i <= min(N, -(T % 10) + T + 100 * ti - t + 108); i += 1) {
          if (i >= 1 && N >= i + 1) {
            S2(T / 10, ti, t, i);
          } else {
            S1(T / 10, ti, t, i);
          }
        }
      }
    }
  } else {
    for (int tt = 0; tt <= T / 10; tt += 1) {
      for (int ti = 0; ti <= N / 100; ti += 1) {
        for (int t = max(2, 10 * tt); t <= min(T, 10 * tt + 9); t += 1) {
          for (int i = max(0, -10 * tt + 100 * ti + t - 9); i <= min(N, 10 * tt + 100 * ti - t + 108); i += 1) {
            if (i >= 1 && N >= i + 1) {
              S2(tt, ti, t, i);
            } else {
              S1(tt, ti, t, i);
            }
          }
        }
      }
    }
  }
}

void stencil_base(float* X, float* Y, int T, int N)
{
	int t, i;
	for (i=0; i<=N; i++) {
		S0(0,0,0,i);
		S0(0,0,1,i);
	}
	for (int t=2; t<=N; t++) {
		S1(0,0,t,0);
		for (i=1; i<N; i++) {
			S2(0,0,t,i);
		}
		S1(0,0,t,N);
		
	}
}

#undef S0
#undef S1
#undef S2
#undef K1
#undef K2
#undef TRAPEZOID

// convolution computation
#define kernel(i) kernel[i]
#define padded_kernel(i) padded_kernel[i]
#define patch(i) patch[i]

void init(long _P0, long _N0, long _K0, long _PK0, float* kernel, float* padded_kernel, float* patch){
	///Parameter checking
	if (!((_P0 >= 1 && _N0 >= 1 && _K0 >= 1 && _PK0 >= 1))) {
		printf("The value of parameters are not valid.\n");
		exit(-1);
	}
	//Memory Allocation
	
	#define S0(i,i1) padded_kernel(i1) = 0.0
	#define S1(i,i1) padded_kernel(i1) = kernel(-_PK0+i1+_K0)
	#define S2(i,i1) padded_kernel(i1) = 0.0
	#define S3(i,i1) patch(i1) = 0.0
	#define S4(i,i1) patch(i1) = 1.0
	#define S5(i,i1) patch(i1) = 0.0
	{
		//Domain
		//{i,i1|i==0 && _P0>=1 && _N0>=1 && _K0>=1 && _PK0>=1 && _PK0>=_K0+i1+1 && i1>=0 && 2_PK0>=i1}
		//{i,i1|i==0 && _K0+i1>=_PK0 && _K0+_PK0>=i1 && _P0>=1 && _N0>=1 && _K0>=1 && _PK0>=1 && i1>=0 && 2_PK0>=i1}
		//{i,i1|i==0 && _P0>=1 && _N0>=1 && _K0>=1 && _PK0>=1 && i1>=_K0+_PK0+1 && i1>=0 && 2_PK0>=i1}
		//{i,i1|i==1 && _P0>=1 && _N0>=1 && _K0>=1 && _PK0>=1 && _PK0>=i1+1 && _N0+2_PK0>=i1+1 && i1>=0}
		//{i,i1|i==1 && i1>=_PK0 && _N0+_PK0>=i1+1 && _P0>=1 && _N0>=1 && _K0>=1 && _PK0>=1 && _N0+2_PK0>=i1+1 && i1>=0}
		//{i,i1|i==1 && _P0>=1 && _N0>=1 && _K0>=1 && _PK0>=1 && i1>=_N0+_PK0 && _N0+2_PK0>=i1+1 && i1>=0}
		int c2;
		for(c2=0;c2 <= -_K0+_PK0-1;c2+=1)
		 {
		 	S0((0),(c2));
		 }
		for(c2=max(0,-_K0+_PK0);c2 <= min(2*_PK0,_K0+_PK0);c2+=1)
		 {
		 	S1((0),(c2));
		 }
		for(c2=_K0+_PK0+1;c2 <= 2*_PK0;c2+=1)
		 {
		 	S2((0),(c2));
		 }
		for(c2=0;c2 <= _PK0-1;c2+=1)
		 {
		 	S3((1),(c2));
		 }
		for(c2=_PK0;c2 <= _N0+_PK0-1;c2+=1)
		 {
		 	S4((1),(c2));
		 }
		for(c2=_N0+_PK0;c2 <= _N0+2*_PK0-1;c2+=1)
		 {
		 	S5((1),(c2));
		 }
	}
	#undef S0
	#undef S1
	#undef S2
	#undef S3
	#undef S4
	#undef S5
	
	//Memory Free
}

//Memory Macros
#undef kernel
#undef padded_kernel
#undef patch

//Local Function Declarations
float reduce_conv_out_1(long, long, int, float*, float*);

//Memory Macros
#define kernel(i) kernel[i]
#define arr(i) arr[i]
#define out(i) out[i]

void conv(long _K0, long _L0, float* kernel, float* arr, float* out){
	///Parameter checking
	if (!((_K0 >= 1 && _L0 >= _K0))) {
		printf("The value of parameters are not valid.\n");
		exit(-1);
	}
	//Memory Allocation
	
	#define S0(i) out(i) = reduce_conv_out_1(_K0,_L0,i,arr,kernel)
	{
		//Domain
		//{i|_K0+i>=0 && _K0+_L0>=i+1 && _K0>=1 && _L0>=_K0 && i>=0 && _L0>=i+1}
		int c1;
		for(c1=0;c1 <= _L0-1;c1+=1)
		 {
		 	S0((c1));
		 }
	}
	#undef S0
	
	//Memory Free
}
float reduce_conv_out_1(long _K0, long _L0, int ip, float* arr, float* kernel){
	float reduceVar = 0;
	#define S1(i,p) reduceVar = (reduceVar)+((arr(i+p))*(kernel(-p+_K0)))
	{
		//Domain
		//{i,p|_K0+ip>=0 && _K0+_L0>=ip+1 && _K0>=1 && _L0>=_K0 && ip>=0 && _L0>=ip+1 && i+p>=0 && _L0>=i+p+1 && i>=0 && _L0>=i+1 && _K0+p>=0 && _K0>=p && _K0+i>=0 && _K0+_L0>=i+1 && ip==i}
		int c2;
		for(c2=max(-_K0,-ip);c2 <= min(_K0,_L0-ip-1);c2+=1)
		 {
		 	S1((ip),(c2));
		 }
	}
	#undef S1
	return reduceVar;
}

//Memory Macros
#undef kernel
#undef arr
#undef out

#define Y_base(t,i) Y_base[((t))*(N+1)+(i)]
#define Y_abft(t,i) Y_abft[((t))*(N+1)+(i)]

int main(int argc, char** argv) {
					
	if (argc < 3) {
		printf("usage: %s T N\n", argv[0]);
		return 1;
	}
	int T = atoi(argv[1]);
	int N = atoi(argv[2]);
	
	float* X = (float*)malloc(sizeof(float)*(N+1));
	float* Y_base = (float*)malloc(sizeof(float)*(T+1)*(N+1));
	float* Y_abft = (float*)malloc(sizeof(float)*(T+1)*(N+1));
	float* weights = (float*)malloc(sizeof(float)*(118));
	float* C1 = (float*)malloc(sizeof(float)*((N/100)*(T/9)));
	float* C2 = (float*)malloc(sizeof(float)*((N/100)*(T/9)));
	
	//srand(time(NULL));
	srand(0);
	
	for (int i=0; i<=N; i++) {
		X(i) = rand()%1000;
	}
	
	long _P0 = 9;
	long _N0 = 100;
	long _K0 = 1;
	long _PK0 = _P0*_K0;
	
	int padded_kernel_L0 = 2*(_PK0)+1;
	int patch_L0 = _N0+2*_PK0;
	
	float* kernel = (float*)malloc(sizeof(float)*(2*_K0+1));
	mallocCheck(kernel, (2*_K0+1), float);
	float* padded_kernel = (float*)malloc(sizeof(float)*(2*_PK0+1));
	mallocCheck(padded_kernel, (2*_PK0+1), float);
	float* padded_kernel_cp = (float*)malloc(sizeof(float)*(2*_PK0+1));
	mallocCheck(padded_kernel_cp, (2*_PK0+1), float);
	float* patch = (float*)malloc(sizeof(float)*(_N0+2*_PK0+0));
	mallocCheck(patch, (_N0+2*_PK0+0), float);
	float* patch_cp = (float*)malloc(sizeof(float)*(_N0+2*_PK0+0));
	mallocCheck(patch_cp, (_N0+2*_PK0+0), float);
	float* _tmp;
	
	#define swap(_A,_B) do {_tmp=_A; _A=_B; _B=_tmp;} while(0)
	#define kernel(i) kernel[i]
	
	kernel(0) = 0.3332;
	kernel(1) = 0.3333;
	kernel(2) = 0.3334;
	
	// initialize the buffers with appropriate padding
	init(_P0, _N0, _K0, _PK0, kernel, padded_kernel, patch);
	
	// precompute the self-convolution of the kernel _P0=9 times
	for (int p=1; p<_P0; p++) {
	    conv(_K0, padded_kernel_L0, kernel, padded_kernel, padded_kernel_cp);
	    swap(padded_kernel, padded_kernel_cp);
	}
	conv(_PK0, patch_L0, padded_kernel, patch, patch_cp);
	swap(patch, patch_cp);
	
	for (int i=0; i<118; i++) {
		W(i) = patch[i];
	}
	
	stencil_base(X, Y_base, T, N);
	stencil_abft(X, Y_abft, C1, C2, weights, T, N);
	
	// sanity check that abft-augmented code still produces the correct answer
	int is_correct = 1;
	for (int t=0; t<=T; t++) {
		for (int i=0; i<=N; i++) {
			if (fabs( (Y_base(t,i)-Y_abft(t,i)) / Y_base(t,i)) > 1e-4) {
				printf("error: Y_base(%d,%d)=%f, Y_abft(%d,%d)=%f\n",t,i,Y_base(t,i),t,i,Y_abft(t,i));
				is_correct = 0;
			}
		}
	}
	if (is_correct) {
		printf("result is correct\n");
	}
}
