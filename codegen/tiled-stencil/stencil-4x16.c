#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>

#define max(x, y)   ((x)>(y) ? (x) : (y))
#define min(x, y)   ((x)>(y) ? (y) : (x))
#define ceild(n,d)  (int)ceil(((double)(n))/((double)(d)))
#define floord(n,d) (int)floor(((double)(n))/((double)(d)))
#define mallocCheck(v,s,d) if ((v) == NULL) { printf("Failed to allocate memory for %s : size=%lu\n", "sizeof(d)*(s)", sizeof(d)*(s)); exit(-1); }

#define X(i) X[i]
#define Y(t,i) Y[((t))*(N+1)+(i)]
#define C1(tt,ti) C1[(N/16)*(tt)+(ti)]
#define C2(tt,ti) C2[(N/16)*(tt)+(ti)]
#define W(i) weights[i]

#define THRESHOLD 1e-6
#define I(tt,ti) fabs((C2(tt,ti)-C1(tt,ti))/C2(tt,ti)) > THRESHOLD // condition when tile computation is correct

#define S0(tt,ti,t,i) Y(t,i) = X(i)
#define S1(tt,ti,t,i) Y(t,i) = Y(t-1,i)
#define S2(tt,ti,t,i) Y(t,i) = 0.3332*Y(t-1,i-1) + 0.3333*Y(t-1,i) + 0.3334*Y(t-1,i+1)
#define K1(tt,ti,t,i) C1(tt,ti) += Y(t,i)
#define K2(tt,ti,t,i) C2(tt,ti) += W((i)-1-16*(ti)+4) * Y(t,i)
#define TRAPEZOID(tt,ti) do { compute_trapezoid(tt, ti, X, Y, C1, C2, weights, T, N); } while(I(tt,ti))

void compute_trapezoid(int tt, int ti, float* X, float* Y, float* C1, float* C2, float* weights, int T, int N)
{
  S2(tt, ti, 4 * tt, 16 * ti - 3);
  K2(tt, ti, 4 * tt, 16 * ti - 3);
  S2(tt, ti, 4 * tt, 16 * ti - 2);
  K2(tt, ti, 4 * tt, 16 * ti - 2);
  S2(tt, ti, 4 * tt, 16 * ti - 1);
  K2(tt, ti, 4 * tt, 16 * ti - 1);
  S2(tt, ti, 4 * tt, 16 * ti);
  K2(tt, ti, 4 * tt, 16 * ti);
  S2(tt, ti, 4 * tt, 16 * ti + 1);
  K2(tt, ti, 4 * tt, 16 * ti + 1);
  S2(tt, ti, 4 * tt, 16 * ti + 2);
  K2(tt, ti, 4 * tt, 16 * ti + 2);
  S2(tt, ti, 4 * tt, 16 * ti + 3);
  K2(tt, ti, 4 * tt, 16 * ti + 3);
  S2(tt, ti, 4 * tt, 16 * ti + 4);
  K2(tt, ti, 4 * tt, 16 * ti + 4);
  S2(tt, ti, 4 * tt, 16 * ti + 5);
  K2(tt, ti, 4 * tt, 16 * ti + 5);
  S2(tt, ti, 4 * tt, 16 * ti + 6);
  K2(tt, ti, 4 * tt, 16 * ti + 6);
  S2(tt, ti, 4 * tt, 16 * ti + 7);
  K2(tt, ti, 4 * tt, 16 * ti + 7);
  S2(tt, ti, 4 * tt, 16 * ti + 8);
  K2(tt, ti, 4 * tt, 16 * ti + 8);
  S2(tt, ti, 4 * tt, 16 * ti + 9);
  K2(tt, ti, 4 * tt, 16 * ti + 9);
  S2(tt, ti, 4 * tt, 16 * ti + 10);
  K2(tt, ti, 4 * tt, 16 * ti + 10);
  S2(tt, ti, 4 * tt, 16 * ti + 11);
  K2(tt, ti, 4 * tt, 16 * ti + 11);
  S2(tt, ti, 4 * tt, 16 * ti + 12);
  K2(tt, ti, 4 * tt, 16 * ti + 12);
  S2(tt, ti, 4 * tt, 16 * ti + 13);
  K2(tt, ti, 4 * tt, 16 * ti + 13);
  S2(tt, ti, 4 * tt, 16 * ti + 14);
  K2(tt, ti, 4 * tt, 16 * ti + 14);
  S2(tt, ti, 4 * tt, 16 * ti + 15);
  K2(tt, ti, 4 * tt, 16 * ti + 15);
  S2(tt, ti, 4 * tt, 16 * ti + 16);
  K2(tt, ti, 4 * tt, 16 * ti + 16);
  S2(tt, ti, 4 * tt, 16 * ti + 17);
  K2(tt, ti, 4 * tt, 16 * ti + 17);
  S2(tt, ti, 4 * tt, 16 * ti + 18);
  K2(tt, ti, 4 * tt, 16 * ti + 18);
  S2(tt, ti, 4 * tt + 1, 16 * ti - 2);
  S2(tt, ti, 4 * tt + 1, 16 * ti - 1);
  S2(tt, ti, 4 * tt + 1, 16 * ti);
  S2(tt, ti, 4 * tt + 1, 16 * ti + 1);
  S2(tt, ti, 4 * tt + 1, 16 * ti + 2);
  S2(tt, ti, 4 * tt + 1, 16 * ti + 3);
  S2(tt, ti, 4 * tt + 1, 16 * ti + 4);
  S2(tt, ti, 4 * tt + 1, 16 * ti + 5);
  S2(tt, ti, 4 * tt + 1, 16 * ti + 6);
  S2(tt, ti, 4 * tt + 1, 16 * ti + 7);
  S2(tt, ti, 4 * tt + 1, 16 * ti + 8);
  S2(tt, ti, 4 * tt + 1, 16 * ti + 9);
  S2(tt, ti, 4 * tt + 1, 16 * ti + 10);
  S2(tt, ti, 4 * tt + 1, 16 * ti + 11);
  S2(tt, ti, 4 * tt + 1, 16 * ti + 12);
  S2(tt, ti, 4 * tt + 1, 16 * ti + 13);
  S2(tt, ti, 4 * tt + 1, 16 * ti + 14);
  S2(tt, ti, 4 * tt + 1, 16 * ti + 15);
  S2(tt, ti, 4 * tt + 1, 16 * ti + 16);
  S2(tt, ti, 4 * tt + 1, 16 * ti + 17);
  S2(tt, ti, 4 * tt + 2, 16 * ti - 1);
  S2(tt, ti, 4 * tt + 2, 16 * ti);
  S2(tt, ti, 4 * tt + 2, 16 * ti + 1);
  S2(tt, ti, 4 * tt + 2, 16 * ti + 2);
  S2(tt, ti, 4 * tt + 2, 16 * ti + 3);
  S2(tt, ti, 4 * tt + 2, 16 * ti + 4);
  S2(tt, ti, 4 * tt + 2, 16 * ti + 5);
  S2(tt, ti, 4 * tt + 2, 16 * ti + 6);
  S2(tt, ti, 4 * tt + 2, 16 * ti + 7);
  S2(tt, ti, 4 * tt + 2, 16 * ti + 8);
  S2(tt, ti, 4 * tt + 2, 16 * ti + 9);
  S2(tt, ti, 4 * tt + 2, 16 * ti + 10);
  S2(tt, ti, 4 * tt + 2, 16 * ti + 11);
  S2(tt, ti, 4 * tt + 2, 16 * ti + 12);
  S2(tt, ti, 4 * tt + 2, 16 * ti + 13);
  S2(tt, ti, 4 * tt + 2, 16 * ti + 14);
  S2(tt, ti, 4 * tt + 2, 16 * ti + 15);
  S2(tt, ti, 4 * tt + 2, 16 * ti + 16);
  S2(tt, ti, 4 * tt + 3, 16 * ti);
  K1(tt, ti, 4 * tt + 3, 16 * ti);
  S2(tt, ti, 4 * tt + 3, 16 * ti + 1);
  K1(tt, ti, 4 * tt + 3, 16 * ti + 1);
  S2(tt, ti, 4 * tt + 3, 16 * ti + 2);
  K1(tt, ti, 4 * tt + 3, 16 * ti + 2);
  S2(tt, ti, 4 * tt + 3, 16 * ti + 3);
  K1(tt, ti, 4 * tt + 3, 16 * ti + 3);
  S2(tt, ti, 4 * tt + 3, 16 * ti + 4);
  K1(tt, ti, 4 * tt + 3, 16 * ti + 4);
  S2(tt, ti, 4 * tt + 3, 16 * ti + 5);
  K1(tt, ti, 4 * tt + 3, 16 * ti + 5);
  S2(tt, ti, 4 * tt + 3, 16 * ti + 6);
  K1(tt, ti, 4 * tt + 3, 16 * ti + 6);
  S2(tt, ti, 4 * tt + 3, 16 * ti + 7);
  K1(tt, ti, 4 * tt + 3, 16 * ti + 7);
  S2(tt, ti, 4 * tt + 3, 16 * ti + 8);
  K1(tt, ti, 4 * tt + 3, 16 * ti + 8);
  S2(tt, ti, 4 * tt + 3, 16 * ti + 9);
  K1(tt, ti, 4 * tt + 3, 16 * ti + 9);
  S2(tt, ti, 4 * tt + 3, 16 * ti + 10);
  K1(tt, ti, 4 * tt + 3, 16 * ti + 10);
  S2(tt, ti, 4 * tt + 3, 16 * ti + 11);
  K1(tt, ti, 4 * tt + 3, 16 * ti + 11);
  S2(tt, ti, 4 * tt + 3, 16 * ti + 12);
  K1(tt, ti, 4 * tt + 3, 16 * ti + 12);
  S2(tt, ti, 4 * tt + 3, 16 * ti + 13);
  K1(tt, ti, 4 * tt + 3, 16 * ti + 13);
  S2(tt, ti, 4 * tt + 3, 16 * ti + 14);
  K1(tt, ti, 4 * tt + 3, 16 * ti + 14);
  S2(tt, ti, 4 * tt + 3, 16 * ti + 15);
  K1(tt, ti, 4 * tt + 3, 16 * ti + 15);
}

void stencil_abft(float* X, float* Y, float* C1, float* C2, float* weights, int T, int N)
{
  /*
   * domain: "[T, N] -> { S1[tt, ti, t, i = N] : T >= 41 and N >= 101 and 16ti <= N and t >= 4tt and 2 <= t <= 18 - N + 4tt + 16ti and t <= 3 + 4tt and t <= T; S1[tt, ti = 0, t, i = 0] : T >= 41 and N >= 101 and t >= 4tt and 2 <= t <= 3 + 4tt and t <= T; S0[tt = 0, ti, t, i] : T >= 41 and N >= 101 and ti >= 0 and 16ti <= N and 0 <= t <= 1 and i >= -3 + 16ti + t and 0 <= i <= 18 + 16ti - t and i <= N; K2[tt, ti, t = 4tt, i] : T >= 41 and N >= 101 and tt > 0 and 4tt <= -4 + T and ti > 0 and 16ti <= -20 + N and -3 + 16ti <= i <= 18 + 16ti and i < N; S2[tt, ti, t, i] : T >= 41 and N >= 101 and ti >= 0 and 16ti <= N and t >= 4tt and 2 <= t <= 3 + 4tt and t <= T and i > 0 and -3 - 4tt + 16ti + t <= i <= 18 + 4tt + 16ti - t and i < N; K1[tt, ti, t = 3 + 4tt, i] : T >= 41 and N >= 101 and tt > 0 and 4tt <= -4 + T and ti > 0 and 16ti <= -20 + N and 16ti <= i <= 15 + 16ti and i < N }"
   * child:
   *   sequence:
   *   - filter: "{ S0[tt, ti, t, i] }"
   *   - filter: "{ S2[tt, ti, t, i]; S1[tt, ti, t, i]; K2[tt, ti, t, i]; K1[tt, ti, t, i] }"
   *     child:
   *       schedule: "[T, N] -> [{ S1[tt, ti, t, i] -> [(tt)]; K2[tt, ti, t, i] -> [(tt)]; S2[tt, ti, t, i] -> [(tt)]; K1[tt, ti, t, i] -> [(tt)] }, { S1[tt, ti, t, i] -> [(ti)]; K2[tt, ti, t, i] -> [(ti)]; S2[tt, ti, t, i] -> [(ti)]; K1[tt, ti, t, i] -> [(ti)] }, { S1[tt, ti, t, i] -> [(t)]; K2[tt, ti, t, i] -> [(t)]; S2[tt, ti, t, i] -> [(t)]; K1[tt, ti, t, i] -> [(t)] }, { S1[tt, ti, t, i] -> [(i)]; K2[tt, ti, t, i] -> [(i)]; S2[tt, ti, t, i] -> [(i)]; K1[tt, ti, t, i] -> [(i)] }]"
   *       options: "[T, N] -> { isolate[[] -> [tt, ti, t, i]] : tt > 0 and 4tt <= -4 + T and ti > 0 and 16ti <= -20 + N; [isolate[] -> unroll[i0]] : 2 <= i0 <= 3 }"
   *       child:
   *         sequence:
   *         - filter: "{ S2[tt, ti, t, i]; S1[tt, ti, t, i] }"
   *         - filter: "{ K2[tt, ti, t, i]; K1[tt, ti, t, i] }"
   */
  for (int ti = 0; ti <= N / 16; ti += 1) {
    for (int t = 0; t <= 1; t += 1) {
      for (int i = max(0, 16 * ti + t - 3); i <= min(N, 16 * ti - t + 18); i += 1) {
        S0(0, ti, t, i);
      }
    }
  }
  for (int ti = 0; ti <= N / 16; ti += 1) {
    for (int t = 2; t <= 3; t += 1) {
      if (ti == 0) {
        S1(0, 0, t, 0);
      }
      for (int i = max(1, 16 * ti + t - 3); i <= min(N, 16 * ti - t + 18); i += 1) {
        if (N >= i + 1) {
          S2(0, ti, t, i);
        } else {
          S1(0, ti, t, N);
        }
      }
    }
  }
  for (int tt = 1; tt < T / 4; tt += 1) {
    for (int t = 4 * tt; t <= 4 * tt + 3; t += 1) {
      S1(tt, 0, t, 0);
      for (int i = 1; i <= 4 * tt - t + 18; i += 1) {
        S2(tt, 0, t, i);
      }
    }
    for (int ti = 1; ti < (N - 4) / 16; ti += 1) {
      TRAPEZOID(tt, ti);
    }
    for (int ti = (N - 4) / 16; ti <= N / 16; ti += 1) {
      for (int t = 4 * tt; t <= 4 * tt + 3; t += 1) {
        for (int i = -4 * tt + 16 * ti + t - 3; i <= min(N - 1, 4 * tt + 16 * ti - t + 18); i += 1) {
          S2(tt, ti, t, i);
        }
        if (4 * tt + 16 * ti + 18 >= N + t) {
          S1(tt, ti, t, N);
        }
      }
    }
  }
  for (int ti = 0; ti <= N / 16; ti += 1) {
    for (int t = -(T % 4) + T; t <= T; t += 1) {
      for (int i = max(0, (T % 4) - T + 16 * ti + t - 3); i <= min(N, -(T % 4) + T + 16 * ti - t + 18); i += 1) {
        if (i >= 1 && N >= i + 1) {
          S2(T / 4, ti, t, i);
        } else {
          S1(T / 4, ti, t, i);
        }
      }
    }
  }
}

void stencil_base(float* X, float* Y, int T, int N)
{
	int t, i;
	for (i=0; i<=N; i++) {
		S0(0,0,0,i);
		S0(0,0,1,i);
	}
	for (int t=2; t<=N; t++) {
		S1(0,0,t,0);
		for (i=1; i<N; i++) {
			S2(0,0,t,i);
		}
		S1(0,0,t,N);
		
	}
}

#undef S0
#undef S1
#undef S2
#undef K1
#undef K2
#undef TRAPEZOID

// convolution computation
#define kernel(i) kernel[i]
#define padded_kernel(i) padded_kernel[i]
#define patch(i) patch[i]

void init(long _P0, long _N0, long _K0, long _PK0, float* kernel, float* padded_kernel, float* patch){
	///Parameter checking
	if (!((_P0 >= 1 && _N0 >= 1 && _K0 >= 1 && _PK0 >= 1))) {
		printf("The value of parameters are not valid.\n");
		exit(-1);
	}
	//Memory Allocation
	
	#define S0(i,i1) padded_kernel(i1) = 0.0
	#define S1(i,i1) padded_kernel(i1) = kernel(-_PK0+i1+_K0)
	#define S2(i,i1) padded_kernel(i1) = 0.0
	#define S3(i,i1) patch(i1) = 0.0
	#define S4(i,i1) patch(i1) = 1.0
	#define S5(i,i1) patch(i1) = 0.0
	{
		//Domain
		//{i,i1|i==0 && _P0>=1 && _N0>=1 && _K0>=1 && _PK0>=1 && _PK0>=_K0+i1+1 && i1>=0 && 2_PK0>=i1}
		//{i,i1|i==0 && _K0+i1>=_PK0 && _K0+_PK0>=i1 && _P0>=1 && _N0>=1 && _K0>=1 && _PK0>=1 && i1>=0 && 2_PK0>=i1}
		//{i,i1|i==0 && _P0>=1 && _N0>=1 && _K0>=1 && _PK0>=1 && i1>=_K0+_PK0+1 && i1>=0 && 2_PK0>=i1}
		//{i,i1|i==1 && _P0>=1 && _N0>=1 && _K0>=1 && _PK0>=1 && _PK0>=i1+1 && _N0+2_PK0>=i1+1 && i1>=0}
		//{i,i1|i==1 && i1>=_PK0 && _N0+_PK0>=i1+1 && _P0>=1 && _N0>=1 && _K0>=1 && _PK0>=1 && _N0+2_PK0>=i1+1 && i1>=0}
		//{i,i1|i==1 && _P0>=1 && _N0>=1 && _K0>=1 && _PK0>=1 && i1>=_N0+_PK0 && _N0+2_PK0>=i1+1 && i1>=0}
		int c2;
		for(c2=0;c2 <= -_K0+_PK0-1;c2+=1)
		 {
		 	S0((0),(c2));
		 }
		for(c2=max(0,-_K0+_PK0);c2 <= min(2*_PK0,_K0+_PK0);c2+=1)
		 {
		 	S1((0),(c2));
		 }
		for(c2=_K0+_PK0+1;c2 <= 2*_PK0;c2+=1)
		 {
		 	S2((0),(c2));
		 }
		for(c2=0;c2 <= _PK0-1;c2+=1)
		 {
		 	S3((1),(c2));
		 }
		for(c2=_PK0;c2 <= _N0+_PK0-1;c2+=1)
		 {
		 	S4((1),(c2));
		 }
		for(c2=_N0+_PK0;c2 <= _N0+2*_PK0-1;c2+=1)
		 {
		 	S5((1),(c2));
		 }
	}
	#undef S0
	#undef S1
	#undef S2
	#undef S3
	#undef S4
	#undef S5
	
	//Memory Free
}

//Memory Macros
#undef kernel
#undef padded_kernel
#undef patch

//Local Function Declarations
float reduce_conv_out_1(long, long, int, float*, float*);

//Memory Macros
#define kernel(i) kernel[i]
#define arr(i) arr[i]
#define out(i) out[i]

void conv(long _K0, long _L0, float* kernel, float* arr, float* out){
	///Parameter checking
	if (!((_K0 >= 1 && _L0 >= _K0))) {
		printf("The value of parameters are not valid.\n");
		exit(-1);
	}
	//Memory Allocation
	
	#define S0(i) out(i) = reduce_conv_out_1(_K0,_L0,i,arr,kernel)
	{
		//Domain
		//{i|_K0+i>=0 && _K0+_L0>=i+1 && _K0>=1 && _L0>=_K0 && i>=0 && _L0>=i+1}
		int c1;
		for(c1=0;c1 <= _L0-1;c1+=1)
		 {
		 	S0((c1));
		 }
	}
	#undef S0
	
	//Memory Free
}
float reduce_conv_out_1(long _K0, long _L0, int ip, float* arr, float* kernel){
	float reduceVar = 0;
	#define S1(i,p) reduceVar = (reduceVar)+((arr(i+p))*(kernel(-p+_K0)))
	{
		//Domain
		//{i,p|_K0+ip>=0 && _K0+_L0>=ip+1 && _K0>=1 && _L0>=_K0 && ip>=0 && _L0>=ip+1 && i+p>=0 && _L0>=i+p+1 && i>=0 && _L0>=i+1 && _K0+p>=0 && _K0>=p && _K0+i>=0 && _K0+_L0>=i+1 && ip==i}
		int c2;
		for(c2=max(-_K0,-ip);c2 <= min(_K0,_L0-ip-1);c2+=1)
		 {
		 	S1((ip),(c2));
		 }
	}
	#undef S1
	return reduceVar;
}

//Memory Macros
#undef kernel
#undef arr
#undef out

#define Y_base(t,i) Y_base[((t))*(N+1)+(i)]
#define Y_abft(t,i) Y_abft[((t))*(N+1)+(i)]

int main(int argc, char** argv) {
					
	if (argc < 3) {
		printf("usage: %s T N\n", argv[0]);
		return 1;
	}
	int T = atoi(argv[1]);
	int N = atoi(argv[2]);
	
	float* X = (float*)malloc(sizeof(float)*(N+1));
	float* Y_base = (float*)malloc(sizeof(float)*(T+1)*(N+1));
	float* Y_abft = (float*)malloc(sizeof(float)*(T+1)*(N+1));
	float* weights = (float*)malloc(sizeof(float)*(22));
	float* C1 = (float*)malloc(sizeof(float)*((N/16)*(T/3)));
	float* C2 = (float*)malloc(sizeof(float)*((N/16)*(T/3)));
	
	//srand(time(NULL));
	srand(0);
	
	for (int i=0; i<=N; i++) {
		X(i) = rand()%1000;
	}
	
	long _P0 = 3;
	long _N0 = 16;
	long _K0 = 1;
	long _PK0 = _P0*_K0;
	
	int padded_kernel_L0 = 2*(_PK0)+1;
	int patch_L0 = _N0+2*_PK0;
	
	float* kernel = (float*)malloc(sizeof(float)*(2*_K0+1));
	mallocCheck(kernel, (2*_K0+1), float);
	float* padded_kernel = (float*)malloc(sizeof(float)*(2*_PK0+1));
	mallocCheck(padded_kernel, (2*_PK0+1), float);
	float* padded_kernel_cp = (float*)malloc(sizeof(float)*(2*_PK0+1));
	mallocCheck(padded_kernel_cp, (2*_PK0+1), float);
	float* patch = (float*)malloc(sizeof(float)*(_N0+2*_PK0+0));
	mallocCheck(patch, (_N0+2*_PK0+0), float);
	float* patch_cp = (float*)malloc(sizeof(float)*(_N0+2*_PK0+0));
	mallocCheck(patch_cp, (_N0+2*_PK0+0), float);
	float* _tmp;
	
	#define swap(_A,_B) do {_tmp=_A; _A=_B; _B=_tmp;} while(0)
	#define kernel(i) kernel[i]
	
	kernel(0) = 0.3332;
	kernel(1) = 0.3333;
	kernel(2) = 0.3334;
	
	// initialize the buffers with appropriate padding
	init(_P0, _N0, _K0, _PK0, kernel, padded_kernel, patch);
	
	// precompute the self-convolution of the kernel _P0=3 times
	for (int p=1; p<_P0; p++) {
	    conv(_K0, padded_kernel_L0, kernel, padded_kernel, padded_kernel_cp);
	    swap(padded_kernel, padded_kernel_cp);
	}
	conv(_PK0, patch_L0, padded_kernel, patch, patch_cp);
	swap(patch, patch_cp);
	
	for (int i=0; i<22; i++) {
		W(i) = patch[i];
	}
	
	stencil_base(X, Y_base, T, N);
	stencil_abft(X, Y_abft, C1, C2, weights, T, N);
	
	// sanity check that abft-augmented code still produces the correct answer
	int is_correct = 1;
	for (int t=0; t<=T; t++) {
		for (int i=0; i<=N; i++) {
			if (fabs( (Y_base(t,i)-Y_abft(t,i)) / Y_base(t,i)) > 1e-4) {
				printf("error: Y_base(%d,%d)=%f, Y_abft(%d,%d)=%f\n",t,i,Y_base(t,i),t,i,Y_abft(t,i));
				is_correct = 0;
			}
		}
	}
	if (is_correct) {
		printf("result is correct\n");
	}
}
