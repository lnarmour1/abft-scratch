## Example vanilla codegen for jac1d1r

This directory contains the simplest most basic codegen for the jac1d1r example.
The `*.iscc` files contain the barvinok script commands to generate the loops for each of the version samples.

Link to online iscc tool:  
https://compsys-tools.ens-lyon.fr/iscc/

