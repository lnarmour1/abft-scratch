#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define max(x, y)   ((x)>(y) ? (x) : (y))
#define min(x, y)   ((x)>(y) ? (y) : (x))
#define floord(n,d) (int)floor(((float)(n))/((float)(d)))

#define T_DISPLAY 2

unsigned int lrand_uint(unsigned int limit);

int t_inject;
int i_inject;
int j_inject;
int bit_to_flip;

void inject(float *val) {
  int *bits;
  bits = (int*)val;
  *bits ^= 1 << bit_to_flip;
}

int main(int argc, char **argv) {

  float dt, dt2, f0, t0, t_total, factor, angle_force;
  float abs_thick, abs_rate;
  float lmargin[2];
  float rmargin[2];
  int i_src, j_src;
  int t, i, j;
  float dp_dxx, dp_dzz, co_dxx, co_dzz;
  int T, NX, NZ, dx, dz;

  #define vp(i,j) 3300.0
  #define force_x(t) _force_x[t]
  #define p(t,i,j) _p[((t)%3)*((NZ+1)*(NX+1))+(i)*(NZ+1)+(j)]
  #define weights(i,j) _weights[(i)*(NZ+1)+(j)]

  // physical parameters
  NX = 400;
  NZ = 400;
  dx = 10;
  dz = 10;

  float *_p = (float*)calloc(3*(NZ+1)*(NX+1), sizeof(float));
  float *_weights = (float*)malloc(sizeof(float)*(NZ+1)*(NX+1));

  t_total = 2*0.55;
  dt = 0.7 * min(dx,dz) / vp(0,0);
  dt2 = dt * dt;
  T = floord(t_total, dt);

  // Create the source pulse
  //
  f0 = 10.0;
  t0 = 1.2 / f0;
  factor = 1e10;
  i_src = floord(NZ, 2);
  j_src = floord(NX, 2);
  float *_force_x = (float*)malloc(sizeof(float)*T);
  for (t=0; t<T; t++) {
    force_x(t) = factor * exp(-1*M_PI*M_PI*f0*f0*(t*dt-t0)*(t*dt-t0)) * dt2 / (dx*dz);
  }

  abs_thick = min(floor(0.15*NX), floor(0.15*NZ));
  abs_rate = 0.3 / abs_thick;
  #define WiLjL(i,j) weights(i,j) = exp(-1*abs_rate*abs_rate*((abs_thick-i)*(abs_thick-i)+(abs_thick-j)*(abs_thick-j)))
  #define WiLjM(i,j) weights(i,j) = exp(-1*abs_rate*abs_rate*((abs_thick-i)*(abs_thick-i)))
  #define WiMjL(i,j) weights(i,j) = exp(-1*abs_rate*abs_rate*((abs_thick-j)*(abs_thick-j)))
  #define WiMjH(i,j) weights(i,j) = exp(-1*abs_rate*abs_rate*((NX-abs_thick-j)*(NX-abs_thick-j)))
  #define WiLjH(i,j) weights(i,j) = exp(-1*abs_rate*abs_rate*((abs_thick-i)*(abs_thick-i)+(NX-abs_thick-j)*(NX-abs_thick-j)))
  #define WiHjL(i,j) weights(i,j) = exp(-1*abs_rate*abs_rate*((NZ-abs_thick-i)*(NZ-abs_thick-i)+(abs_thick-j)*(abs_thick-j)))
  #define WiHjM(i,j) weights(i,j) = exp(-1*abs_rate*abs_rate*((NZ-abs_thick-i)*(NZ-abs_thick-i)))
  #define WiHjH(i,j) weights(i,j) = exp(-1*abs_rate*abs_rate*((NZ-abs_thick-i)*(NZ-abs_thick-i)+(NX-abs_thick-j)*(NX-abs_thick-j)))
  for (int c0 = abs_thick; c0 < NZ - abs_thick + 1; c0 += 1)
    for (int c1 = abs_thick; c1 < NX - abs_thick + 1; c1 += 1) 
      weights(c0, c1) = 1.0;
  for (int c0 = 0; c0 < abs_thick; c0 += 1) {
    for (int c1 = 0; c1 < abs_thick; c1 += 1)
      WiLjL(c0, c1);
    for (int c1 = abs_thick; c1 < NX - abs_thick + 1; c1 += 1) 
      WiLjM(c0, c1);
    for (int c1 = NX - abs_thick + 1; c1 <= NX; c1 += 1)
      WiLjH(c0, c1);
  }
  for (int c0 = NZ - abs_thick + 1; c0 <= NZ; c0 += 1) {
    for (int c1 = 0; c1 < abs_thick; c1 += 1)
      WiHjL(c0, c1);
    for (int c1 = abs_thick; c1 < NX - abs_thick + 1; c1 += 1) 
      WiHjM(c0, c1);
    for (int c1 = NX - abs_thick + 1; c1 <= NX; c1 += 1)
      WiHjH(c0, c1);
  }
  for (int c0 = abs_thick; c0 < NX - abs_thick + 1; c0 += 1) {
    for (int c1 = 0; c1 < abs_thick; c1 += 1) 
      WiMjL(c0, c1);
    for (int c1 = NX - abs_thick + 1; c1 <= NX; c1 += 1)
      WiMjH(c0, c1);
  }

  // Coefficients for derivatives
  co_dxx = 1.0 / (dx*dx);
  co_dzz = 1.0 / (dz*dz);

  #ifdef ERROR_SIM
  // Error injection setup
  t_inject = 180; //lrand_uint(T);
  i_inject = 150; //lrand_uint(NZ);
  j_inject = 215; //lrand_uint(NX);
  bit_to_flip = 25; //argc>2 ? atoi(argv[1]) : lrand_uint(31);
  fprintf(stderr, "t_inj = %d\n", t_inject);
  fprintf(stderr, "i_inj = %d\n", i_inject);
  fprintf(stderr, "j_inj = %d\n", j_inject);
  fprintf(stderr, " bit -> %d\n", bit_to_flip);
  #endif

  float q0 = vp(i,j)*vp(i,j) * dt2 * co_dxx;
  float q1 = vp(i,j)*vp(i,j) * dt2 * co_dzz;
  #define coeff(i,j) _coeff[((i)+1)*3+((j)+1)]
  float _coeff[9];
 
  coeff( 0, 0)  = 2 * (1 - q0 - q1); 
  coeff( 0,-1) = q0;
  coeff( 0, 1) = q0;
  coeff(-1, 0) = q1;
  coeff( 1, 0) = q1;


  for (t=2; t<=T; t++) {

    // U(t) = 2*U(t-1) - U(t-2) + G dt2/rho;
    for (i=1; i<=NZ-1; i++) 
      for (j=1; j<=NX-1; j++) 
        p(t,i,j) = coeff(-1, 0) * weights(i,j) * p(t-1,i-1,j+0) +\
                   coeff( 0,-1) * weights(i,j) * p(t-1,i+0,j-1) +\
                   coeff( 0, 0) * weights(i,j) * p(t-1,i+0,j+0) +\
                   coeff( 1, 0) * weights(i,j) * p(t-1,i+1,j+0) +\
                   coeff( 0, 1) * weights(i,j) * p(t-1,i+0,j+1) +\
                           (-1) * weights(i,j) * p(t-2,i+0,j+0);

    // Source energy pulse
    p(t,i_src,j_src) += force_x(t-2);

    #ifdef ERROR_SIM
    if (t==t_inject) {
      inject(&p(t_inject, i_inject, j_inject));
    }
    #endif

    // Output
    if (t%T_DISPLAY == 0) {
      printf("%d,", t);
      for (i=0; i<=NZ; i++) {
        for (j=0; j<=NX; j++) {
          printf("%f,", p(t,i,j));
        }
      }
      printf("\n");
    }
  }

}
