from matplotlib import pyplot as plt
from matplotlib import animation
import numpy as np


with open('/tmp/.c-output.csv', 'r') as f:
    raw = [[v for v in l.strip('\n').split(',')] for l in f.readlines()]

Ts = []
data = []
for line in raw:
    t = int(line[0])
    d = [float(v) for v in line[1:-1]]
    Ts.append(t)
    data.append(d)

Ts = np.array(Ts)

NZ, NX = 401, 401

for t in range(len(Ts)):
    data[t] = np.array(data[t])
    data[t].resize((NZ, NX))

fig = plt.figure()
im = plt.imshow(data[0], extent=[0,NX,NZ,0], vmax=50, cmap='jet')
plt.xlabel('i')
plt.ylabel('j')

def init():
    im.set_data(data[0])
    return [im]

def animate(i):
    im.set_array(data[i])
    #plt.title('error injection @ [t,i,j]=[180,215,150]\nt = {}'.format(Ts[i]))
    plt.title('t = {}'.format(Ts[i]))
    return [im]

anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=len(Ts), interval=1, blit=True)

anim.save('err-inject.gif', writer='imagemagick', fps=60)

plt.show()
