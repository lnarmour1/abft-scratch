## slightly more realistic example

This is a C-implementation of the code here:
https://github.com/ovcharenkoo/WaveProp_in_MATLAB/tree/master/acoustic_2D_FDTD_wave_propagation

Example:
```
$ ./run.sh
```
![example](example.gif)
