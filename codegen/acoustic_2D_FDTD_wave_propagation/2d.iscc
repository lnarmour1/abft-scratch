Domain := [T,N,cT,cI,cJ,cW,cS,ct] -> {
  C[t,i,j,p] : 0<=i,j<=N and 0<=t<=T;
  K[t,i,j,p] : 0<=i,j<=N and 0<=t<=T;
  B1[t,i,j,p] : 0<=i,j<=N and 0<=t<=T;
  B2[t,i,j,p] : 0<=i,j<=N and 0<=t<=T;
  R1[t,i,j,p] : 0<=i,j<=N and 0<=t<=T;
  R2[t,i,j,p] : 0<=i,j<=N and 0<=t<=T;
  Y[t,i,j,p] : 0<=i,j<=N and 0<=t<=T;
};

Stencil := [T,N,cT,cI,cJ,cW,cS,ct] -> {
  S0[t,i,j] : 0<=i,j<=N and t=0;
  S1[t,i,j] : 0<i,j<N and 1<=t<=T;
};

Checksum := [T,N,cT,cI,cJ,cW,cS,ct] -> {
  K[cT,i,j,0] : cI<=i<=cI+cW;
};

M := [T,N,cT,cI,cJ,cW,cS,ct] -> { 
    K[t,i,j,p]->C[t,i,j,p] : t=cT;

    K[t,i,j,p]->K[t-1,i,j,p+1] : 0<(i-cI)+(t-cT) and (i-cI)-(t-cT)<cW;
    K[t,i,j,p]->B1[t-1,i,j,p+1] : 0=(i-cI)+(t-cT);
    K[t,i,j,p]->B2[t-1,i,j,p+1] : (i-cI)-(t-cT)=cW;
    K[t,i,j,p]->R1[t-1,i-1,j,p+1] : 0=(i-cI)+(t-cT);
    K[t,i,j,p]->R2[t-1,i+1,j,p+1] : (i-cI)-(t-cT)=cW;
    K[t,i,j,p]->Y[t-2,i,j,p+1] : (i-cI)-(t-cT-1)=cW;

    K[t,i,j,p]->K[t-1,i,j,p+1] : 0<(j-cJ)+(t-cT) and (j-cJ)-(t-cT)<cW;
    K[t,i,j,p]->B1[t-1,i,j,p+1] : 0=(j-cJ)+(t-cT);
    K[t,i,j,p]->B2[t-1,i,j,p+1] : (j-cJ)-(t-cT)=cW;
    K[t,i,j,p]->R1[t-1,i,j-1,p+1] : 0=(j-cJ)+(t-cT);
    K[t,i,j,p]->R2[t-1,i,j+1,p+1] : (j-cJ)-(t-cT)=cW;
    K[t,i,j,p]->Y[t-2,i,j,p+1] : (j-cJ)-(t-cT-1)=cW;

    C[t,i,j,p]->C[t,i,j,p] ;
    B1[t,i,j,p]->B1[t,i,j,p] ;
    B2[t,i,j,p]->B2[t,i,j,p] ;
    R1[t,i,j,p]->R1[t,i,j,p] ;
    R2[t,i,j,p]->R2[t,i,j,p] ;
    Y[t,i,j,p]->Y[t,i,j,p] ;
};

ret := pow M;
Mp := ret[0];
exact := ret[1];
#print "Is power expansion exact?"; 
#print exact;
S := [T,N,cT,cI,cJ,cW,cS,ct]->{[s] : s=cS};
Mp := unwrap(Mp(S));

Checksum := Mp(Checksum) * Domain;

Checksum_row := [T,N,cT,cI,cJ,cW,cS,ct]->{
  C[t,i,j,p]->C[t,i,j,p] : t=ct;
  K[t,i,j,p]->K[t,i,j,p] : t=ct;
  B1[t,i,j,p]->B1[t,i,j,p] : t=ct;
  B2[t,i,j,p]->B2[t,i,j,p] : t=ct;
  R1[t,i,j,p]->R1[t,i,j,p] : t=ct;
  R2[t,i,j,p]->R2[t,i,j,p] : t=ct;
  Y[t,i,j,p]->Y[t,i,j,p] : t=ct;
}(Checksum);

Checksum_origins := [T,N,cT,cI,cJ,cW,cS,ct] -> {
  ABFT[t,i,j] : 0<=t<=T and 0<=i,j<=N;
};
Checksum_origins := {ABFT[t,i,j]->ABFT[t%__Sp1__,i%__Wm2S__,j%__Wm2S__,floor(t/__Sp1__),floor(i/__Wm2S__),floor(j/__Wm2S__)]}(Checksum_origins);
Checksum_origins := {ABFT[t,i,j,tt,ti,tj]->ABFT[t,tt,ti,tj]}(Checksum_origins);
Checksum_origins := {ABFT[t,tt,ti,tj]->ABFT[__Sp1__*tt+t,__Wm2S__*ti,__Wm2S__*tj,tt,ti,tj]}(Checksum_origins);
Checksum_origins := {ABFT[t,i,j,tt,ti,tj]->ABFT[t,i,j,tt,ti,tj,__Sp1__*tt+__S__]}(Checksum_origins);

Schedule_checksum_row := [T,N,cT,cI,cJ,cW,cS,ct] -> {
   K[t,i,j,p]->[t,i,j];
  B1[t,i,j,p]->[t,i,j];
  B2[t,i,j,p]->[t,i,j];
  R1[t,i,j,p]->[t,i,j];
  R2[t,i,j,p]->[t,i,j];
   Y[t,i,j,p]->[t,i,j];
   C[t,i,j,p]->[t,i,j];
};

Schedule := [T,N,cT,cI,cJ,cW,cS,ct] -> {
  S0[t,i,j]->[0,t,0,i,j];
  S1[t,i,j]->[1,t,0,i,j];
  ABFT[t,i,tt,ti,tj,_cT]->[1,t,1,ti,tj];
};

codegen (Schedule_checksum_row * Checksum_row);
print("###delimit###");
codegen (Schedule * (Stencil + Checksum_origins));


