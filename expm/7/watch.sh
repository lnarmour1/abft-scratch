#!/bin/bash

total=0
for S in {5..200..5}; do for r in {0..50}; do total=$((total+1)); done; done;

function all {
  for m in `cat fish-machines.txt | grep -v '^#'`
  do
    for p in `echo all-$m.*.log`
    do 
      threshold=`echo $p | sed "s~all-$m.\(.*\).log~\1~"`
      lines=`cat all-$m.$threshold.log | wc -l`
      echo "$m.$threshold,->,$lines/$total (`bc -l <<< 'scale=2; 100*'$lines'/'$total''`%%)"
    done
  done
}

all | column -t -s,
