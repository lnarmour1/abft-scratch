#include<stdio.h>
#include<math.h>

#define max(x, y)   ((x)>(y) ? (x) : (y))
#define min(x, y)   ((x)>(y) ? (y) : (x))
#define floord(n,d) (int)floor(((float)(n))/((float)(d)))

int __t;
int __i;

int in_patch(int tt, int ti, int T, int N, int cS, int cW, int t_inj, int i_inj) {
  
  int cT = (tt+1)*(cS);
  int cI = (ti)*(cW-cS-cS);

  int ret = 0;

  //#define S(t, i) printf("%d,%d\n",t,i) 
  #define S(t_inj, i_inj) ret = 1

  if (cW >= 0 && cS >= 1 && cS + t_inj >= cT && t_inj >= 0 && cT >= t_inj && T >= t_inj && cW + 2 * t_inj + 2 >= 2 * cT && i_inj >= 0 && cT + i_inj >= cI + t_inj && cT + cI + cW >= t_inj + i_inj && N >= i_inj && t_inj + i_inj + 2 >= cT + cI && cI + cW + t_inj + 2 >= cT + i_inj)
    S(t_inj, i_inj);


  return ret;
}


int main()
{

  int T = 10;
  int N = 50;
  int cS = 4;
  int cW = 16;

  __t = 5;
  __i = 4;


  #define P_origin(tt,ti) printf("%d,%d <-- %d\n", tt, ti, in_patch(tt,ti,T,N,cS,cW,__t,__i))
  //#define P_origin(tt,ti) in_patch(tt,ti,T,N,cS,cW,__t,__i)

  int Sp1 = cS+1;
  int Wm2S = cW - cS - cS;

  for (int c0 = 0; c0 <= floord(T, Sp1); c0 += 1) 
    for (int c1 = 0; c1 <= floord(N, Wm2S); c1 += 1) {
      P_origin(c0, c1);
    }



}
