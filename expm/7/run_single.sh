#!/bin/bash

if [[ -z "$1" || -z "$2" || -z "$3" || -z "$4" ]]; then
  echo "usage: $0 S W T N [bit]"
  exit 1;
fi

S=$1
W=$2
T=$3
N=$4
bit=$5

if [[ ! -f ./bin/jac.$S.$W.abft ]]; then
  ./lcc $S $W > /dev/null
fi
./bin/jac.$S.$W.abft $T $N $bit 2>/dev/null
