#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <unistd.h>
#include<sys/time.h>

struct timeval ttime;
double elapsed_time;
#define start_timer() gettimeofday(&ttime, NULL); elapsed_time = (((double) ttime.tv_sec) + ((double) ttime.tv_usec)/1000000)
#define stop_timer() gettimeofday(&ttime, NULL); elapsed_time = (((double) ttime.tv_sec) + ((double) ttime.tv_usec)/1000000) - elapsed_time

#define max(x, y)   ((x)>(y) ? (x) : (y))
#define min(x, y)   ((x)>(y) ? (y) : (x))
#define floord(n,d) (int)floor(((float)(n))/((float)(d)))

#define output(t,i) out[(t)%2][i]
#define input(i) in[i]

#define W __W__
#define S __S__
#define cW __W__
#define cS __S__

float alpha_to_the[S+1];
float THRESHOLD;
float DEBUG = 0;

float pre_flip_val;
float post_flip_val;

struct Patch {
  long tt;
  long ti;
  long t_min;
  long t_max;
  long i_min;
  long i_max;
  float checksum;
  float invariant;
  float R1;
  float R2;
  float B1;
  float B2;
  float K;
  float difference;
  int covers_injection;
};

struct Result {
  long num_patches;
  struct Patch *patches;
};

long VERBOSE;

int t_inject;
int i_inject;
int bit_to_flip;
#ifdef ERR_INJECTION
#define do_inject(t,i) (t==t_inject && i==i_inject) ? 1 : 0
#else
#define do_inject(t,i) 0
#endif

unsigned int lrand_uint(unsigned int limit);
float lrand_float(float limit);
double lrand_double(double limit);

void inject(float *val, int t, int i) {
  int *bits;
  bits = (int*)val;
  pre_flip_val = *val;
  *bits ^= 1 << bit_to_flip;
  post_flip_val= *((float*)bits);
}

// N+1 data grid over T timesteps
void jac1d1r(float *in, float **out, float w0, float w1, float w2, long T, long N) {
  #define S0(t,i) output(t,i) = input(i)
  #define S1(t,i) output(t,i) = output(t-1,i)
  #define S2(t,i) do { output(t,i) = w0*output(t-1,i-1) + w1*output(t-1,i) + w2*output(t-1,i+1); if (do_inject(t,i)==1) {inject(&output(t,i),t,i);}} while(0)
  #define S3(t,i) output(t,i) = output(t-1,i)
  {
    for (int c3 = 0; c3 <= N; c3 += 1)
      S0(0, c3);
    if (N <= -1) {
      for (int c1 = 1; c1 <= T; c1 += 1) {
        S3(c1, N);
        S1(c1, 0);
      }
    } else {
      for (int c1 = 0; c1 <= T; c1 += 1) {
        if (c1 >= 1) {
          S1(c1, 0);
          for (int c3 = 1; c3 < N; c3 += 1)
            S2(c1, c3);
          S3(c1, N);
        }
      }
    }
  }
}

static inline void abft(int ct, int cI, int tt, int ti, int cT, int T, int N, float *check, float **out, int zT, int zI) {

  #define checksum(tt,ti) check[0*zI*zT+(tt)*zI+(ti)]
  #define invariant(tt,ti) check[1*zI*zT+(tt)*zI+(ti)]
  #define inv_K(tt,ti) check[2*zI*zT+(tt)*zI+(ti)]
  #define inv_B1(tt,ti) check[3*zI*zT+(tt)*zI+(ti)]
  #define inv_B2(tt,ti) check[4*zI*zT+(tt)*zI+(ti)]
  #define inv_R1(tt,ti) check[5*zI*zT+(tt)*zI+(ti)]
  #define inv_R2(tt,ti) check[6*zI*zT+(tt)*zI+(ti)]

  #define C(t,i,p) checksum(tt,ti) += output(t,i)
  #define K(t,i,p) inv_K(tt,ti) += output(t,i)
  #define B1(t,i,p) inv_B1(tt,ti) += alpha_to_the[p-1] * output(t,i)
  #define B2(t,i,p) inv_B2(tt,ti) += alpha_to_the[p-1] * output(t,i)
  #define R1(t,i,p) inv_R1(tt,ti) += alpha_to_the[p-1] * output(t,i)
  #define R2(t,i,p) inv_R2(tt,ti) += alpha_to_the[p-1] * output(t,i)

  // ISCC abft codegen below
  // ISCC abft codegen above
  
}

void jac1d1r_abft(float *in, float **out, float w0, float w1, float w2, int T, int N, int zT, int zI, float *check, struct Result *result) {

  #define S0(t,i) output(t,i) = input(i)
  #define S1(t,i) output(t,i) = output(t-1,i)
  #define S2(t,i) do { output(t,i) = w0*output(t-1,i-1) + w1*output(t-1,i) + w2*output(t-1,i+1); if (do_inject(t,i)==1) {inject(&output(t,i),t,i);}} while(0)
  #define S3(t,i) output(t,i) = output(t-1,i)

  #define ABFT(ct,cI,tt,ti,cT) abft(ct,cI,tt,ti,cT,T,N,check,out,zT,zI)

  // ISCC stencil+abft+caller codegen below
  // ISCC stencil+abft+caller codegen above

  // multiple by coeffs
  for (int tt=0; tt<zT; tt++) 
    for (int ti=0; ti<zI; ti++) 
      invariant(tt,ti) = (w0)*inv_R1(tt,ti) + (w0+w1)*inv_B1(tt,ti) + (alpha_to_the[S])*inv_K(tt,ti) + (w1+w2)*inv_B2(tt,ti) + (w2)*inv_R2(tt,ti);

}

int main(int argc, char* argv[]) {
  
  if (argc < 3) {
    printf("usage: %s T N [bit]\n", argv[0]);
    return 1;
  }
  long T = atol(argv[1]);
  long N = atol(argv[2]);
  if (argc >= 4)
    bit_to_flip = atoi(argv[3]);
  else
    bit_to_flip = lrand_uint(31);  

  if (!(0<T && 0<N)) {
    printf("unsupported parameter values, must be positive.\n");
    return 1;
  }    
  if (!(W<=N)) {
    printf("unsupported parameter values, expecting N>=%d\n", W);
    return 1;
  }    
  if (!(T>=S)) {
    printf("unsupported parameter values, expecting T>=%d\n", S);
    return 1;
  }

  if (getenv("THRESHOLD"))
    THRESHOLD = atof(getenv("THRESHOLD"));
  else
    THRESHOLD = 0.0001;

  if (getenv("VERBOSE"))
    VERBOSE = atoi(getenv("VERBOSE"));
  else
    VERBOSE = -1;

  float w0 = 0.3333;
  float w1 = 0.3333;
  float w2 = 1 - w0 - w1;

  float alpha = w0 + w1 + w2;
  alpha_to_the[0] = 1.0;
  for (int i=1; i<=S; i++) {
    alpha_to_the[i] = alpha_to_the[i-1] * alpha;
  }


  float *in = (float*)malloc((N+1)*sizeof(float));
  float *blob = (float*)malloc((N+1)*3*sizeof(float));
  float *out[3] = { &blob[0], &blob[N], &blob[2*N] };
  #ifdef CHECK
  blob = (float*)malloc((N+1)*2*sizeof(float));
  float *out_check[2] = { &blob[0], &blob[N] };
  #endif

  // initialize random input
  for (int i=0; i<=N; i++) {
    in[i] = lrand_float(10);
  }

  // allocate memory for checksums and initialize to zero
  int zI = (N / (W-S-S));
  int zT = (T / (S+1));
  float *check = (float*)calloc(7*(zI+1)*(zT+1), sizeof(float));

  start_timer();
  jac1d1r(in, out, w0, w1, w2, T, N);
  stop_timer();
  float baseline_time = elapsed_time;

  #ifdef ERR_INJECTION
  if (getenv("T_INJ"))
    t_inject = atol(getenv("T_INJ"));
  else
    t_inject = S+1 + lrand_uint(T-(2*(S+1)));
  
  if (getenv("I_INJ"))
    i_inject = atol(getenv("I_INJ"));
  else
    i_inject = W + lrand_uint(N-(2*W));
  #endif

  struct Result result;
  struct Patch *patches = (struct Patch *)malloc((zI)*(zT)*sizeof(struct Patch));
  result.patches = patches;
  result.num_patches = (zI)*(zT);

  start_timer();
  #ifdef KERNEL_WITH_ABFT
  // run kernel + checksum
  jac1d1r_abft(in, out, w0, w1, w2, T, N, zT, zI, check, &result);
  #else
  // run vanilla protypical kernel
  jac1d1r(in, out, w0, w1, w2, T, N);
  #endif
  stop_timer();

  float overhead = 100*fabsf((baseline_time-elapsed_time)/baseline_time);

  // check for differences
  int k = 0;
  for (int tt=0; tt<zT; tt++) {
    for (int ti=0; ti<zI; ti++) {
      float difference = fabsf((checksum(tt,ti) - invariant(tt,ti)) / checksum(tt,ti));
      result.patches[k].tt = tt;
      result.patches[k].ti = ti;
      int t_min = tt*(S+1);
      int i_min = ti*(W-S-S);
      result.patches[k].t_min = t_min;
      result.patches[k].t_max = min(t_min+S, T);
      result.patches[k].i_min = i_min;
      result.patches[k].i_max = min(i_min+W, N);
      
      result.patches[k].difference = difference;
      result.patches[k].checksum = checksum(tt,ti);
      result.patches[k].invariant = invariant(tt,ti);
      result.patches[k].R1 = inv_R1(tt,ti);
      result.patches[k].R2 = inv_R2(tt,ti);
      result.patches[k].B1 = inv_B1(tt,ti);
      result.patches[k].B2 = inv_B2(tt,ti);
      result.patches[k].K = inv_K(tt,ti);
      k++;
      if (VERBOSE >= 2) {
        printf("[tt,ti]=[%d,%d] -> relative difference: %E, ", tt, ti, difference);
        printf(" checksum(%d,%d) -> %f\n", tt, ti, checksum(tt,ti));
        printf("invariant(%d,%d) -> %f\n", tt, ti, invariant(tt,ti));
        printf("R1(%d,%d) -> %f\n", tt, ti, inv_R1(tt,ti));
        printf("B1(%d,%d) -> %f\n", tt, ti, inv_B1(tt,ti));
        printf(" K(%d,%d) -> %f\n", tt, ti, inv_K(tt,ti));
        printf("B2(%d,%d) -> %f\n", tt, ti, inv_B2(tt,ti));
        printf("R2(%d,%d) -> %f\n", tt, ti, inv_R2(tt,ti));
        printf("\n");
      }
    }
  }

  struct Patch *p;
  for (int i=0; i<result.num_patches; i++) {
    p = &result.patches[i];
    if (t_inject == p->t_max && p->i_min<=i_inject && i_inject<=p->i_max)
      p->covers_injection = 1;
    else if (p->t_min<=t_inject && t_inject<p->t_max) {
      int t_offset = p->t_max - t_inject;
      if (p->i_min-1+t_offset<i_inject && i_inject<p->i_max+1-t_offset) 
        p->covers_injection = 1;
    }
  }

  // process accuracy
  long TP = 0;
  long TN = 0;
  long FP = 0;
  long FN = 0;
  for (int i=0; i<result.num_patches; i++) {
    p = &result.patches[i];
    int above_threshold = p->difference > THRESHOLD;
    if ( (p->covers_injection) &&  above_threshold)
      TP++;
    if ( (p->covers_injection) && !above_threshold)
      FN++;
    if (!(p->covers_injection) &&  above_threshold)
      FP++;
    if (!(p->covers_injection) && !above_threshold)
      TN++;
  }
  // TP, TN, FP, FN rates
  float TPR = (TP + 0.0) / (TP + FN);
  float FNR = (FN + 0.0) / (TP + FN);
  float TNR = (TN + 0.0) / (TN + FP);
  float FPR = (FP + 0.0) / (TN + FP);
  float acc = (TP + TN + 0.0) / (TP + TN + FP + FN);

  if (DEBUG)
    for (int i=0; i<result.num_patches; i++) {
      p = &result.patches[i];
      if (!(p->covers_injection))
        continue;
      if (p->tt==0 || p->tt==zT-1 || p->ti==0 || p->ti==zI-1)
        continue;
      printf("%d,%d:\n", p->tt, p->ti);
      printf("  checksum:   %E\n", p->checksum);
      printf("  invariant:  %E\n", p->invariant);
      printf("  R1:         %E\n", p->R1);
      printf("  B1:         %E\n", p->B1);
      printf("  K:          %E\n", p->K);
      printf("  B2:         %E\n", p->B2);
      printf("  R2:         %E\n", p->R2);
      printf("  difference: %E", p->difference);
      if (p->covers_injection)
        printf(" ***");
      printf("\n");
      printf("  t_range:    [%d,%d]\n", p->t_min, p->t_max);
      printf("  i_range:    [%d,%d]\n", p->i_min, p->i_max);
    }

  if (VERBOSE >= 1) {
    printf("summary\n---\n");
    printf("T:                    %d\n", T);
    printf("N:                    %d\n", N);
    printf("S (patch depth):      %d\n", S);
    printf("W (patch width):      %d\n", W);
    printf("injection site (t,i): %d,%d\n", t_inject, i_inject);
    printf("bit flipped:          %d\n", bit_to_flip);
    printf("pre-flip value:       %E\n", pre_flip_val);
    printf("post-flip value:      %E\n", post_flip_val);
    printf("baseline (sec):       %f\n", baseline_time);
    printf("exec time (sec):      %f\n", elapsed_time);
    printf("overhead (%%):         %.2f\n", overhead);
    printf("threshold:            %E\n", THRESHOLD);
    printf("TP:  %d\n", TP);
    printf("TN:  %d\n", TN);
    printf("FP:  %d\n", FP);
    printf("FN:  %d\n", FN);
    printf("TPR: %5.2f%%\n", 100*TPR);
    printf("TNR: %5.2f%%\n", 100*TNR);
    printf("FPR: %5.2f%%\n", 100*FPR);
    printf("FNR: %5.2f%%\n", 100*FNR);
    printf("acc: %5.2f%%\n", 100*acc);
    printf("enclosing patches (tt,ti -> checksum-invariant-diff):\n");
    for (int i=0; i<result.num_patches; i++) {
      p = &result.patches[i];
      if (!(p->covers_injection))
        continue;
      printf("  %d,%d -> %E\n", p->tt, p->ti, p->difference);
    }
  } else {
    fprintf(stderr, "T,N,S,W,");
    fprintf(stderr, "baseline_time,exec_time,overhead,t_inj,i_inj,bit,");
    fprintf(stderr, "threshold,TP,FP,TN,FN\n");
    printf("%d,%d,%d,%d,", T, N, S, W);
    printf("%f,%f,%f,%d,%d,%d,", baseline_time, elapsed_time, overhead, t_inject, i_inject, bit_to_flip);
    printf("%E,%d,%d,%d,%d\n", THRESHOLD, TP, FP, TN, FN);
  }



  #ifdef CHECK
  // sanity checking
  jac1d1r(in, out_check, w0, w1, w2, T, N);
  for (int t=0; t<2; t++)
  for (int i=0; i<=N; i++) {
    float difference = fabsf(out[t][i] - out_check[t][i]);
    if (difference > 0.00001)
      printf("%d,%d --> %f,%f\n", t, i, out[t][i], out_check[t][i]);
  }
  #endif 

}
