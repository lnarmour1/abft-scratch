#!/bin/bash

cd ~/git/abft-scratch/expm/7

FILE=all-__MACHINE__.__THRESHOLD__.log
BIT=__BIT__

echo "run, type, S, W, T, N, baseline, t, i, bit flipped, num errors, threshold, time, overhead (%)" > $FILE
for S in {5..200..5}
do
  for r in {0..100}
  do
    echo "$r,$(./run_single.sh $S 1000 $BIT abft.inject)" >> $FILE
  done
done
