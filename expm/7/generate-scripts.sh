#!/bin/bash

bit=31

mkdir -p script-gen

for m in `cat fish-machines.txt`
do
  for threshold in 0.0001 0.00001 0.000001
  do
    FILE=script-gen/run_all_$m.$threshold.sh
    cp templates/run_all.sh $FILE
    sed -i "s~__MACHINE__~$m~" $FILE
    sed -i "s~__BIT__~$bit~" $FILE
    sed -i "s~__THRESHOLD__~$threshold~" $FILE
  done
  bit=$((bit-1))
  if [[ "$bit" -lt 10 ]]; then
    break
  fi
done
