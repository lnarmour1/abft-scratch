#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <unistd.h>
#include<sys/time.h>

struct timeval ttime;
double elapsed_time;
#define start_timer() gettimeofday(&ttime, NULL); elapsed_time = (((double) ttime.tv_sec) + ((double) ttime.tv_usec)/1000000)
#define stop_timer() gettimeofday(&ttime, NULL); elapsed_time = (((double) ttime.tv_sec) + ((double) ttime.tv_usec)/1000000) - elapsed_time

#define max(x, y)   ((x)>(y) ? (x) : (y))
#define min(x, y)   ((x)>(y) ? (y) : (x))
#define floord(n,d) (int)floor(((float)(n))/((float)(d)))

#define output(t,i) out[(t)%2][i]
#define input(i) in[i]

#define W 200
#define S 4
#define cW 200
#define cS 4

float alpha_to_the[S+1];
float THRESHOLD;

int t_inject;
int i_inject;
int bit_to_flip;
#ifdef ERR_INJECTION
#define do_inject(t,i) (t==t_inject && i==i_inject) ? 1 : 0
#else
#define do_inject(t,i) 0
#endif

unsigned int lrand_uint(unsigned int limit);
float lrand_float(float limit);
double lrand_double(double limit);

void inject(float *val, int t, int i) {
  //bit_to_flip = 28; //13 + lrand_uint(31-13);
  int *bits;
  bits = (int*)val;
  //float o1 = *val;
  *bits ^= 1 << bit_to_flip;
  //float n1 = *((float*)bits);
  //printf("%d,%d flipped bit %d (%f -> %f), ", t, i, bit_to_flip, o1, n1); 
  //printf("%d,%d,%d,", t, i, bit_to_flip); 
}

// N+1 data grid over T timesteps
void jac1d1r(float *in, float **out, float w0, float w1, float w2, long T, long N) {
  #define M2(t,i) do { if (do_inject(t,i)==1) {inject(&output(t,i),t,i);} output(t,i) = w0*output(t-1,i-1) + w1*output(t-1,i) + w2*output(t-1,i+1); } while(0)
  long t, i;
  for (i=0; i<=N; i++)
    output(0,i) = input(i);
  for (t=1; t<=T; t++) {
    output(t,0) = output(t-1,0);
    for (i=1; i<N; i++)
      M2(t,i);
    output(t,N) = output(t-1,N);
  }
}

static inline void abft(int ct, int cI, int tt, int ti, int cT, int T, int N, float *check, float **out, int zT, int zI) {

  #define checksum(tt,ti) check[0*zI*zT+(tt)*zI+(ti)]
  #define invariant(tt,ti) check[1*zI*zT+(tt)*zI+(ti)]
  #define inv_K(tt,ti) check[2*zI*zT+(tt)*zI+(ti)]
  #define inv_B1(tt,ti) check[3*zI*zT+(tt)*zI+(ti)]
  #define inv_B2(tt,ti) check[4*zI*zT+(tt)*zI+(ti)]
  #define inv_R1(tt,ti) check[5*zI*zT+(tt)*zI+(ti)]
  #define inv_R2(tt,ti) check[6*zI*zT+(tt)*zI+(ti)]

  #define C(t,i,p) checksum(tt,ti) += output(t,i)
  #define K(t,i,p) inv_K(tt,ti) += output(t,i)
  #define B1(t,i,p) inv_B1(tt,ti) += alpha_to_the[p-1] * output(t,i)
  #define B2(t,i,p) inv_B2(tt,ti) += alpha_to_the[p-1] * output(t,i)
  #define R1(t,i,p) inv_R1(tt,ti) += alpha_to_the[p-1] * output(t,i)
  #define R2(t,i,p) inv_R2(tt,ti) += alpha_to_the[p-1] * output(t,i)

  // ISCC abft codegen below
  {
    if (cT + cI >= ct + 2 && cT >= ct + 1 && cS + ct >= cT && ct >= 0 && T >= ct && N + ct + 2 >= cT + cI && cW + 2 * ct + 2 >= 2 * cT)
      R1(ct, cT + cI - ct - 2, cT - ct);
    if (cT >= ct + 1 && cT + cI >= ct + 1 && cS + ct >= cT && ct >= 0 && T >= ct && N + ct + 1 >= cT + cI && cW + 2 * ct + 2 >= 2 * cT) {
      B1(ct, cT + cI - ct - 1, cT - ct);
    } else if (cT >= 0 && T >= cT && cS >= 1 && ct == cT) {
      for (int c1 = max(0, cI); c1 <= min(N, cI + cW); c1 += 1)
        C(cT, c1, 0);
    }
    if (cS >= 1 && cT >= cS && T + cS >= cT && cS + ct == cT)
      for (int c1 = max(0, cI + cS); c1 <= min(N, cI + cW - cS); c1 += 1)
        K(cT - cS, c1, cS);
    if (cT >= ct + 1 && N + cT >= cI + cW + ct + 1 && cS + ct >= cT && ct >= 0 && T >= ct && cI + cW + ct + 1 >= cT && cW + 2 * ct + 2 >= 2 * cT)
      B2(ct, -cT + cI + cW + ct + 1, cT - ct);
    if (N + cT >= cI + cW + ct + 2 && cT >= ct + 1 && cS + ct >= cT && ct >= 0 && T >= ct && cI + cW + ct + 2 >= cT && cW + 2 * ct + 2 >= 2 * cT)
      R2(ct, -cT + cI + cW + ct + 2, cT - ct);
  }
  // ISCC abft codegen above
  
}

int jac1d1r_abft(float *in, float **out, float w0, float w1, float w2, int T, int N, int zT, int zI, float *check) {

  #define S0(t,i) output(t,i) = input(i)
  #define S1(t,i) output(t,i) = output(t-1,i)
  #define S2(t,i) do { output(t,i) = w0*output(t-1,i-1) + w1*output(t-1,i) + w2*output(t-1,i+1); if (do_inject(t,i)==1) {inject(&output(t,i),t,i);}} while(0)
  #define S3(t,i) output(t,i) = output(t-1,i)

  #define ABFT(ct,cI,tt,ti,cT) abft(ct,cI,tt,ti,cT,T,N,check,out,zT,zI)

  // ISCC stencil+abft+caller codegen below
  {
    for (int c3 = 0; c3 <= N; c3 += 1)
      S0(0, c3);
    if (N <= -1) {
      for (int c1 = 1; c1 <= T; c1 += 1) {
        S3(c1, N);
        S1(c1, 0);
      }
    } else {
      for (int c1 = 0; c1 <= T; c1 += 1) {
        if (c1 >= 1) {
          S1(c1, 0);
          for (int c3 = 1; c3 < N; c3 += 1)
            S2(c1, c3);
          S3(c1, N);
        }
        for (int c3 = 0; c3 <= floord(N, 192); c3 += 1)
          ABFT(c1, 192 * c3, c1 / 5, c3, -(c1 % 5) + c1 + 4);
      }
    }
  }
  // ISCC stencil+abft+caller codegen above

  // multiple by coeffs
  for (int tt=0; tt<zT; tt++) 
    for (int ti=0; ti<zI; ti++) 
      invariant(tt,ti) = (w0)*inv_R1(tt,ti) + (w0+w1)*inv_B1(tt,ti) + (alpha_to_the[S])*inv_K(tt,ti) + (w1+w2)*inv_B2(tt,ti) + (w2)*inv_R2(tt,ti);

  // check for errors
  int n_errors = 0;
  for (int tt=1; tt<zT-1; tt++) 
    for (int ti=1; ti<zI-1; ti++) {
      float rel_error = fabsf((checksum(tt,ti) - invariant(tt,ti)) / checksum(tt,ti));
      if (rel_error > THRESHOLD) {
        n_errors++;
//        printf("[tt,ti]=[%d,%d] -> relative error: %E, ", tt, ti, rel_error);
//        printf(" checksum(%d,%d) -> %f\n", tt, ti, checksum(tt,ti));
//        printf("invariant(%d,%d) -> %f\n", tt, ti, invariant(tt,ti));
//        printf("R1(%d,%d) -> %f\n", tt, ti, inv_R1(tt,ti));
//        printf("B1(%d,%d) -> %f\n", tt, ti, inv_B1(tt,ti));
//        printf(" K(%d,%d) -> %f\n", tt, ti, inv_K(tt,ti));
//        printf("B2(%d,%d) -> %f\n", tt, ti, inv_B2(tt,ti));
//        printf("R2(%d,%d) -> %f\n", tt, ti, inv_R2(tt,ti));
//        printf("\n");
      }
    }
  return n_errors;
}

int main(int argc, char* argv[]) {
  
  if (argc < 4) {
    printf("usage: %s T N bit [threshold]\n", argv[0]);
    return 1;
  }

  long T = atol(argv[1]);
  long N = atol(argv[2]);
  bit_to_flip = atoi(argv[3]);
  if (argc > 4)
    THRESHOLD = atof(argv[4]);
  else
    THRESHOLD = 0.0001;
    

  if (!(0<T && 0<N)) {
    printf("unsupported parameter values, must be positive.\n");
    return 1;
  }    
  if (!(W<=N)) {
    printf("unsupported parameter values, expecting N>=%d\n", W);
    return 1;
  }    
  if (!(T>=S)) {
    printf("unsupported parameter values, expecting T>=%d\n", S);
    return 1;
  }

  float w0 = 0.3333;
  float w1 = 0.3333;
  float w2 = 1 - w0 - w1;

  float alpha = w0 + w1 + w2;
  alpha_to_the[0] = 1.0;
  for (int i=1; i<=S; i++) {
    alpha_to_the[i] = alpha_to_the[i-1] * alpha;
  }


  float *in = (float*)malloc((N+1)*sizeof(float));
  float *blob = (float*)malloc((N+1)*3*sizeof(float));
  float *out[3] = { &blob[0], &blob[N], &blob[2*N] };
  #ifdef CHECK
  blob = (float*)malloc((N+1)*2*sizeof(float));
  float *out_check[2] = { &blob[0], &blob[N] };
  #endif

  // initialize random input
  for (int i=0; i<=N; i++) {
    in[i] = lrand_float(10);
  }

  // allocate memory for checksums and initialize to zero
  int zI = (N / (W-S-S));
  int zT = (T / (S+1));
  float *check = (float*)calloc(7*(zI+1)*(zT+1), sizeof(float));

  //fprintf(stderr, "baseline, t, i, bit flipped, num errors, time, overhead\n");
  
  start_timer();
  jac1d1r(in, out, w0, w1, w2, T, N);
  stop_timer();
  printf("%f,",elapsed_time);
  float baseline_time = elapsed_time;

  #ifdef ERR_INJECTION
  t_inject = lrand_uint(T);
  i_inject = lrand_uint(N);
  #endif

  int n_errors = 0;

  start_timer();
  #ifdef KERNEL_WITH_ABFT
  // run kernel + checksum
  n_errors = jac1d1r_abft(in, out, w0, w1, w2, T, N, zT, zI, check);
  #else
  // run vanilla protypical kernel
  jac1d1r(in, out, w0, w1, w2, T, N);
  #endif
  stop_timer();

  #ifdef ERR_INJECTION
  printf("%d,%d,%d,", t_inject, i_inject, bit_to_flip);
  #else
  printf("-1,-1,-1,");
  #endif

  printf("%d,%.10f,%f,%.2f\n", n_errors, THRESHOLD, elapsed_time, 100*fabsf((baseline_time-elapsed_time)/baseline_time));

  #ifdef CHECK
  // sanity checking
  jac1d1r(in, out_check, w0, w1, w2, T, N);
  for (int t=0; t<2; t++)
  for (int i=0; i<=N; i++) {
    float difference = fabsf(out[t][i] - out_check[t][i]);
    if (difference > 0.00001)
      printf("%d,%d --> %f,%f\n", t, i, out[t][i], out_check[t][i]);
  }
  #endif 

}
