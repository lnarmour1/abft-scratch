#!/bin/bash

cd ~/git/abft-scratch/expm/6.200

FILE=all-__MACHINE__.__THRESHOLD__.log
BIT=__BIT__
THRESHOLD=__THRESHOLD__

echo "run, type, S, W, T, N, baseline, t, i, bit flipped, num errors, threshold, time, overhead (%)" > $FILE
for S in {4..40..2}
do
  for r in {0..50}
  do
    echo "$r,$(./run_single.sh $S 200 $BIT $THRESHOLD abft.inject)" >> $FILE
  done
done
