#!/bin/bash


for m in `cat fish-machines.txt | grep -v '^#'`
do
  echo $m
  ssh $m tmux kill-session -t abft
done
