#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <unistd.h>
#include<sys/time.h>
#include<omp.h>

struct timeval ttime;
double elapsed_time1;
double elapsed_time2;
#define start_timer(t) gettimeofday(&ttime, NULL); t = (((double) ttime.tv_sec) + ((double) ttime.tv_usec)/1000000)
#define stop_timer(t) gettimeofday(&ttime, NULL); t = (((double) ttime.tv_sec) + ((double) ttime.tv_usec)/1000000) - t

#define max(x, y)   ((x)>(y) ? (x) : (y))
#define min(x, y)   ((x)>(y) ? (y) : (x))
#define floord(n,d) (int)floor(((float)(n))/((float)(d)))

#define output(t,i) out[((t)%2)*(N+1)+(i)]
#define input(i) in[i]

#define W 50
#define S 5

unsigned int lrand_uint(unsigned int limit);
float lrand_float(float limit);
double lrand_double(double limit);

// N+1 data grid over T timesteps
void jac1d1r(float *in, float *out, float w0, float w1, float w2, long T, long N) {
  long t, i;
  for (i=0; i<=N; i++)
    output(0,i) = input(i);
  for (t=1; t<=T; t++) {
    output(t,0) = output(t-1,0);
    for (i=1; i<N; i++)
      output(t,i) = w0*output(t-1,i-1) + w1*output(t-1,i) + w2*output(t-1,i+1);
    output(t,N) = output(t-1,N);
  }
}

void inject(float *out, int N, double time_window) {
  int j = lrand_uint(N);
  float *val = &out[j];
  int bit_to_flip = lrand_uint(32);

  // it takes roughly time_window time to process all T timesteps of the stencil
  // sleep for X secs for some random 
  double duration = lrand_double(1.0) * time_window * 1000000; //microseconds
  usleep(duration);

  int *bits;
  bits = (int*)val;
  *bits ^= 1 << bit_to_flip;
  *(bits+N) ^= 1 << bit_to_flip;

  printf("out[%d] flipped bit %d\n", j, bit_to_flip); 
}

void jac1d1r_abft(float *in, float *out, float w0, float w1, float w2, int T, int N, int zT, int zI, float *check) {
  float alpha = w0 + w1 + w2;

  float alpha_to_the[S+1];
  alpha_to_the[0] = 1.0;
  for (int i=1; i<=S; i++) {
    alpha_to_the[i] = alpha_to_the[i-1] * alpha;
  }

  #define S0(t,i) output(t,i) = input(i)
  #define S1(t,i) output(t,i) = output(t-1,i)
  #define S2(t,i) output(t,i) = w0*output(t-1,i-1) + w1*output(t-1,i) + w2*output(t-1,i+1)
  #define S3(t,i) output(t,i) = output(t-1,i)

  #define checksum(tt,ti) check[0*zT*zI+(tt)*(zI)+(ti)]
  #define invariant(tt,ti) check[1*zT*zI+(tt)*(zI)+(ti)]
  #define inv_K(tt,ti) check[2*zT*zI+(tt)*(zI)+(ti)]
  #define inv_B1(tt,ti) check[3*zT*zI+(tt)*(zI)+(ti)]
  #define inv_B2(tt,ti) check[4*zT*zI+(tt)*(zI)+(ti)]
  #define inv_R1(tt,ti) check[5*zT*zI+(tt)*(zI)+(ti)]
  #define inv_R2(tt,ti) check[6*zT*zI+(tt)*(zI)+(ti)]
  
  #define C(t,i,p,tt,ti) checksum(tt,ti) += output(t,i)
  #define K(t,i,p,tt,ti) inv_K(tt,ti) += output(t,i)
  #define B1(t,i,p,tt,ti) inv_B1(tt,ti) += alpha_to_the[p-1] * output(t,i)
  #define B2(t,i,p,tt,ti) inv_B2(tt,ti) += alpha_to_the[p-1] * output(t,i)
  #define R1(t,i,p,tt,ti) inv_R1(tt,ti) += alpha_to_the[p-1] * output(t,i)
  #define R2(t,i,p,tt,ti) inv_R2(tt,ti) += alpha_to_the[p-1] * output(t,i)

  // ISCC codegen below
  if (zT >= 1 && zI >= 1) {
    for (int c3 = 0; c3 <= N; c3 += 1)
      S0(0, c3);
    if (N <= -1) {
      for (int c1 = 1; c1 <= T; c1 += 1) {
        S3(c1, N);
        S1(c1, 0);
      }
    } else {
      for (int c1 = 0; c1 <= T; c1 += 1) {
        if (c1 >= 1) {
          S1(c1, 0);
          for (int c3 = 1; c3 < N; c3 += 1)
            S2(c1, c3);
          S3(c1, N);
        }
        if (N >= c1 + 41 && c1 <= 4) {
          if (c1 == 0) {
            for (int c3 = 0; c3 <= 40; c3 += 1)
              K(0, c3, 5, 0, 0);
            for (int c3 = 41; c3 <= min(42, N); c3 += 1) {
              if (c3 == 42) {
                R2(0, 42, 5, 0, 0);
              } else {
                B2(0, 41, 5, 0, 0);
              }
            }
          }
          for (int c3 = -c1 + 43; c3 <= min(N, 45 * zI - c1 - 46); c3 += 1) {
            if ((c1 + c3 + 2) % 45 == 0) {
              R1(c1, c3, -c1 + 5, 0, (c1 + c3 + 2) / 45);
            } else {
              if ((c1 - c3 - 3) % 45 == 0)
                R2(c1, c3, -c1 + 5, 0, (-c1 + c3 - 42) / 45);
              if ((c1 + c3 + 1) % 45 == 0) {
                B1(c1, c3, -c1 + 5, 0, (c1 + c3 + 1) / 45);
              } else if (c1 == 0 && (c3 - 41) % 45 >= 4) {
                K(0, c3, 5, 0, c3 / 45);
              }
            }
            if ((c1 - c3 - 4) % 45 == 0)
              B2(c1, c3, -c1 + 5, 0, (-c1 + c3 - 41) / 45);
          }
          for (int c3 = max(max(45 * zI - c1 - 45, c1 + 41), -c1 + 43); c3 <= min(N, 45 * zI - c1 - 3); c3 += 1) {
            if ((c1 - c3 - 3) % 45 == 0) {
              R2(c1, c3, -c1 + 5, 0, (-c1 + c3 - 42) / 45);
            } else if (c1 == 0 && 45 * zI >= c3 + 5) {
              K(0, c3, 5, 0, zI - 1);
            } else if ((c1 - c3 - 4) % 45 == 0) {
              B2(c1, c3, -c1 + 5, 0, (-c1 + c3 - 41) / 45);
            }
          }
          for (int c3 = 45 * zI - c1 - 2; c3 <= min(N, 45 * zI - c1 - 1); c3 += 1) {
            if (c1 + c3 + 2 == 45 * zI) {
              R1(c1, 45 * zI - c1 - 2, -c1 + 5, 0, zI);
              if (c1 == 1)
                B2(1, 45 * zI - 3, 4, 0, zI - 1);
            } else {
              if (c1 == 1)
                R2(1, 45 * zI - 2, 4, 0, zI - 1);
              B1(c1, 45 * zI - c1 - 1, -c1 + 5, 0, zI);
            }
          }
          if (c1 >= 2) {
            for (int c3 = 45 * zI + c1 - 4; c3 <= min(N, 45 * zI + c1 - 3); c3 += 1) {
              if (c3 + 3 == 45 * zI + c1) {
                R2(c1, 45 * zI + c1 - 3, -c1 + 5, 0, zI - 1);
              } else {
                B2(c1, 45 * zI + c1 - 4, -c1 + 5, 0, zI - 1);
              }
            }
          } else if (c1 == 0) {
            for (int c3 = 45 * zI; c3 <= min(N, 45 * zI + 40); c3 += 1)
              K(0, c3, 5, 0, zI);
          }
          for (int c3 = 45 * zI + c1 + 41; c3 <= min(N, 45 * zI + c1 + 42); c3 += 1) {
            if (c3 == 45 * zI + c1 + 42) {
              R2(c1, 45 * zI + c1 + 42, -c1 + 5, 0, zI);
            } else {
              B2(c1, 45 * zI + c1 + 41, -c1 + 5, 0, zI);
            }
          }
        } else if (N + c1 >= 43 && c1 <= 4 && c1 + 40 >= N) {
          R1(c1, -c1 + 43, -c1 + 5, 0, 1);
          if (N + c1 >= 44)
            B1(c1, -c1 + 44, -c1 + 5, 0, 1);
        } else if (c1 >= 5 && T >= c1 + 1 && 5 * zT + 4 >= c1 && N >= (c1 % 5) + 41) {
          if (N >= (c1 % 5) + 41 && c1 % 5 >= 1) {
            for (int c3 = -(c1 % 5) + 43; c3 <= min(min(N, -(c1 % 5) + 45 * zI - 46), (c1 % 5) + 85); c3 += 1) {
              if (((c1 % 5) + c3 + 2) % 45 == 0)
                R1(c1, c3, -(c1 % 5) + 5, c1 / 5, ((c1 % 5) + c3 + 2) / 45);
              if (c3 >= 41 && c3 <= 45 && (c1 - c3 + 1) % 5 == 0) {
                B2(c1, c3, -c3 + 46, (c1 - c3 + 41) / 5, 0);
              } else {
                if (c3 >= 42 && c3 <= 46 && (c1 - c3 + 2) % 5 == 0)
                  R2(c1, c3, -c3 + 47, (c1 - c3 + 42) / 5, 0);
                if (((c1 % 5) + c3 + 1) % 45 == 0)
                  B1(c1, c3, -(c1 % 5) + 5, c1 / 5, ((c1 % 5) + c3 + 1) / 45);
              }
            }
            if (zI == 2)
              for (int c3 = max(max(43, -5 * zT + c1 + 41), -(c1 % 5) + 45); c3 <= min(min(46, N), c1 + 37); c3 += 1) {
                if (c3 <= 45 && (c1 - c3 + 1) % 5 == 0) {
                  B2(c1, c3, -c3 + 46, (c1 - c3 + 41) / 5, 0);
                } else if ((c1 - c3 + 2) % 5 == 0) {
                  R2(c1, c3, -c3 + 47, (c1 - c3 + 42) / 5, 0);
                }
              }
          }
          if (c1 % 5 >= 1) {
            for (int c3 = (c1 % 5) + 86; c3 <= min(N, 45 * zI - 7); c3 += 1) {
              if (((c1 % 5) + c3 + 2) % 45 == 0)
                R1(c1, c3, -(c1 % 5) + 5, c1 / 5, ((c1 % 5) + c3 + 2) / 45);
              if (((c1 % 5) - c3 - 4) % 45 == 0) {
                B2(c1, c3, -(c1 % 5) + 5, c1 / 5, (-(c1 % 5) + c3 - 41) / 45);
              } else {
                if (((c1 % 5) - c3 - 3) % 45 == 0)
                  R2(c1, c3, -(c1 % 5) + 5, c1 / 5, (-(c1 % 5) + c3 - 42) / 45);
                if (((c1 % 5) + c3 + 1) % 45 == 0)
                  B1(c1, c3, -(c1 % 5) + 5, c1 / 5, ((c1 % 5) + c3 + 1) / 45);
              }
            }
          } else {
            for (int c3 = 0; c3 <= min(N, 45 * zI - 7); c3 += 1) {
              if ((c3 + 2) % 45 == 0) {
                R1(c1, c3, 5, c1 / 5, (c3 + 2) / 45);
              } else if ((c3 + 4) % 45 == 0) {
                B2(c1, c3, 5, c1 / 5, (c3 - 41) / 45);
              } else if ((c3 + 3) % 45 == 0) {
                R2(c1, c3, 5, c1 / 5, (c3 - 42) / 45);
              } else if (c3 <= 40 || (c3 + 4) % 45 >= 4) {
                K(c1, c3, 5, c1 / 5, c3 <= 40 ? 0 : (c3 + 4) / 45);
              }
              if (c3 <= 45)
                C(c1, c3, 0, (c1 / 5) - 1, 0);
              for (int c8 = max(1, floord(c3 - 1, 45)); c8 <= (c3 + 5) / 45; c8 += 1)
                C(c1, c3, 0, (c1 / 5) - 1, c8);
              if ((c3 + 1) % 45 == 0)
                B1(c1, c3, 5, c1 / 5, (c3 + 1) / 45);
            }
            if (c1 == 5)
              for (int c3 = 45 * zI - 6; c3 <= min(N, 45 * zI - 3); c3 += 1) {
                if (c3 + 4 == 45 * zI) {
                  B2(5, 45 * zI - 4, 5, 1, zI - 1);
                } else if (c3 + 3 == 45 * zI) {
                  R2(5, 45 * zI - 3, 5, 1, zI - 1);
                } else {
                  K(5, c3, 5, 1, zI - 1);
                }
                if (c3 >= 40) {
                  if (zI == 1)
                    C(5, c3, 0, 0, 0);
                  for (int c8 = max(1, zI - 1); c8 <= floord(c3 + 5, 45); c8 += 1)
                    C(5, c3, 0, 0, c8);
                } else {
                  C(5, 39, 0, 0, 0);
                }
              }
            if (zI == 1)
              for (int c3 = max(39, -c1 + 48); c3 <= min(41, -N + 83); c3 += 1) {
                if (c3 == 41) {
                  B2(c1, 41, 5, c1 / 5, 0);
                } else {
                  K(c1, c3, 5, c1 / 5, 0);
                }
                for (int c8 = 0; c8 <= min(1, c3 - 39); c8 += 1)
                  C(c1, c3, 0, (c1 / 5) - 1, c8);
              }
          }
          if (N >= (c1 % 5) + 41)
            for (int c3 = max(max(45 * zI - 6, -N + 45 * zI + 39), 45 * zI - c1 + 3); c3 <= min(min(N, 45 * zI - 1), 5 * zT + 45 * zI - c1 - 1); c3 += 1) {
              if (45 * zI >= c3 + 2 && (c1 + c3 + 2) % 5 == 0)
                R1(c1, c3, -45 * zI + c3 + 7, ((c1 + c3 + 2) / 5) - 9 * zI, zI);
              if (c3 + 4 >= 45 * zI && (c1 - c3 + 1) % 5 == 0) {
                B2(c1, c3, 45 * zI - c3 + 1, ((c1 - c3 - 4) / 5) + 9 * zI, zI - 1);
              } else if (c3 + 3 >= 45 * zI && (c1 - c3 + 2) % 5 == 0) {
                R2(c1, c3, 45 * zI - c3 + 2, ((c1 - c3 - 3) / 5) + 9 * zI, zI - 1);
              } else if (45 * zI >= c3 + 5 && c1 % 5 == 0) {
                K(c1, c3, 5, c1 / 5, zI - 1);
              }
              if (c3 >= 40 && c1 % 5 == 0) {
                if (zI == 1)
                  C(c1, c3, 0, (c1 / 5) - 1, 0);
                for (int c8 = max(1, zI - 1); c8 <= floord(c3 + 5, 45); c8 += 1)
                  C(c1, c3, 0, (c1 / 5) - 1, c8);
              } else if (zI == 1 && c3 == 39 && c1 % 5 == 0) {
                C(c1, 39, 0, (c1 / 5) - 1, 0);
              }
              if (c3 + 5 >= 45 * zI && (c1 + c3 + 1) % 5 == 0)
                B1(c1, c3, -45 * zI + c3 + 6, ((c1 + c3 + 1) / 5) - 9 * zI, zI);
            }
          if (zI == 1 && c1 >= 5 * zT + 2)
            for (int c3 = -5 * zT + c1 + 41; c3 <= min(44, N); c3 += 1) {
              if (5 * zT + c3 == c1 + 41) {
                B2(c1, -5 * zT + c1 + 41, 5 * zT - c1 + 5, zT, 0);
              } else {
                R2(5 * zT + 2, 44, 3, zT, 0);
              }
            }
          if (zI == 1 && c1 % 5 >= 1)
            for (int c3 = 45; c3 <= min(min(46, N), c1 + 37); c3 += 1) {
              if (c3 == 45 && (c1 + 1) % 5 == 0) {
                B2(c1, 45, 1, (c1 - 4) / 5, 0);
              } else if ((c1 - c3 + 2) % 5 == 0) {
                R2(c1, c3, -c3 + 47, (c1 - c3 + 42) / 5, 0);
              }
            }
          if (c1 >= 5 * zT + 2)
            for (int c3 = max(-5 * zT + 45 * zI + c1 - 4, -5 * zT + c1 + 86); c3 <= min(N, 45 * zI - 1); c3 += 1) {
              if (5 * zT + c3 + 4 == 45 * zI + c1) {
                B2(c1, -5 * zT + 45 * zI + c1 - 4, 5 * zT - c1 + 5, zT, zI - 1);
              } else {
                R2(5 * zT + 2, 45 * zI - 1, 3, zT, zI - 1);
              }
            }
          if (zI >= 2)
            for (int c3 = 45 * zI; c3 <= min(N, (c1 % 5) + 45 * zI - 3); c3 += 1) {
              if (c3 == 45 * zI && (c1 + 1) % 5 == 0) {
                B2(c1, 45 * zI, 1, (c1 - 4) / 5, zI - 1);
              } else {
                R2(c1, c3, 45 * zI - c3 + 2, ((c1 - c3 - 3) / 5) + 9 * zI, zI - 1);
              }
            }
          if (c1 % 5 == 0)
            for (int c3 = 45 * zI; c3 <= min(N, 45 * zI + 40); c3 += 1) {
              K(c1, c3, 5, c1 / 5, zI);
              for (int c8 = (c3 - 1) / 45; c8 <= zI; c8 += 1)
                C(c1, c3, 0, (c1 / 5) - 1, c8);
            }
          for (int c3 = max(45 * zI + 41, -5 * zT + 45 * zI + c1 + 41); c3 <= min(min(N, 45 * zI + 46), 45 * zI + c1 + 37); c3 += 1) {
            if (45 * zI + 45 >= c3 && (c1 - c3 + 1) % 5 == 0) {
              B2(c1, c3, 45 * zI - c3 + 46, ((c1 - c3 + 41) / 5) + 9 * zI, zI);
            } else if (c3 >= 45 * zI + 42 && (c1 - c3 + 2) % 5 == 0) {
              R2(c1, c3, 45 * zI - c3 + 47, ((c1 - c3 + 42) / 5) + 9 * zI, zI);
            }
            if (45 * zI + 45 >= c3 && c1 % 5 == 0)
              C(c1, c3, 0, (c1 / 5) - 1, zI);
          }
          if (c1 == 5)
            for (int c3 = 45 * zI + 43; c3 <= min(N, 45 * zI + 45); c3 += 1)
              C(5, c3, 0, 0, zI);
        } else if (c1 >= 5 && T >= c1 + 1 && 5 * zT + 4 >= c1 && (c1 % 5) + N >= 43 && (c1 % 5) + 40 >= N) {
          if (zI >= 2) {
            R1(c1, -(c1 % 5) + 43, -(c1 % 5) + 5, (c1 + 5) / 5 - 1, 1);
          } else {
            R1(c1, -((c1 + 5) % 5) + 43, -((c1 + 5) % 5) + 5, (c1 + 5) / 5 - 1, 1);
          }
          if ((c1 % 5) + N >= 44)
            B1(c1, -(c1 % 5) + 44, -(c1 % 5) + 5, (c1 + 5) / 5 - 1, 1);
        } else if (T >= 5 * zT + 6 && c1 == 5 * zT + 5) {
          for (int c3 = 0; c3 <= min(N, 45 * zI + 45); c3 += 1) {
            if (c3 <= 45)
              C(5 * zT + 5, c3, 0, zT, 0);
            for (int c8 = max(1, floord(c3 - 1, 45)); c8 <= min(zI, (c3 + 5) / 45); c8 += 1)
              C(5 * zT + 5, c3, 0, zT, c8);
          }
        } else if (N <= 40 && c1 == 0) {
          for (int c3 = 0; c3 <= N; c3 += 1)
            K(0, c3, 5, 0, 0);
        } else if (N <= 40 && T >= c1 + 1 && 5 * zT >= c1 && c1 % 5 == 0) {
          for (int c3 = 0; c3 <= N; c3 += 1) {
            C(c1, c3, 0, (c1 / 5) - 1, 0);
            if (N == 40 && c3 == 40)
              C(c1, 40, 0, (c1 / 5) - 1, 1);
            K(c1, c3, 5, c1 / 5, 0);
          }
        } else if (5 * zT + 5 >= T && c1 == T && T % 5 == 0) {
          for (int c3 = 0; c3 <= min(N, 45 * zI + 45); c3 += 1) {
            if (c3 <= 45)
              C(T, c3, 0, (T / 5) - 1, 0);
            for (int c8 = max(1, floord(c3 - 1, 45)); c8 <= min(zI, (c3 + 5) / 45); c8 += 1)
              C(T, c3, 0, (T / 5) - 1, c8);
          }
        }
      }
    }
  }
  // ISCC codegen above

  for (int tt=0; tt<zT; tt++)
    for (int ti=1; ti<zI-1; ti++) {
      // multiple by coeffs
      invariant(tt,ti) = (w0)*inv_R1(tt,ti) + (w0+w1)*inv_B1(tt,ti) + (alpha_to_the[S])*inv_K(tt,ti) + (w1+w2)*inv_B2(tt,ti) + (w2)*inv_R2(tt,ti);
  }
}


int main(int argc, char* argv[]) {
  
  const char* verbose = getenv("VERBOSE");
  const char* rseed = getenv("RSEED");
  if (argc < 3) {
    printf("usage: %s T N\n", argv[0]);
    return 1;
  }

  long T = atol(argv[1]);
  long N = atol(argv[2]);

  if (!(0<T && 0<N)) {
    printf("unsupported parameter values, must be positive.\n");
    return 1;
  }    
  if (!(32<N)) {
    printf("unsupported parameter values, expecting 32<N\n");
    return 1;
  }    
  if (!(T>5)) {
    printf("unsupported parameter values, expecting T>10\n");
    return 1;
  }    
  if (!(T%S==0)) {
    printf("unsupported parameter values, expecting T to be a multiple of %d\n", S);
    return 1;
  }    

  float w0 = 0.3333;
  float w1 = 0.3333;
  float w2 = 1 - w0 - w1;

  float *in = (float*)malloc(sizeof(float)*(N+1));
  float *out = (float*)calloc((N+1)*2, sizeof(float));
  #ifdef CHECK
  float *out_check = (float*)calloc((N+1)*2, sizeof(float));
  #endif

  // initialize random input
  for (int i=0; i<=N; i++) {
    in[i] = lrand_float(10);
  }

  // allocate memory for checksums and initialize to zero
  int zI = (N / (W-S));
  int zT = (T / S);
  float *check = (float*)calloc(7 * zT * zI, sizeof(float));

  #ifdef ERR_INJECTION
  // get average running time
  start_timer(elapsed_time1);
  for (int r=0; r<3; r++)
    jac1d1r_abft(in, out, w0, w1, w2, T, N, zT, zI, check);
  stop_timer(elapsed_time1);

  // reset checksums
  for (int i=0; i<7*zT*zI; i++)
    check[i] = 0.0;
  #endif

  #pragma omp parallel 
  {
    int thread_num = omp_get_thread_num();
    if (thread_num == 0) {
      start_timer(elapsed_time2);
      #ifdef KERNEL_WITH_ABFT
      // run kernel + checksum
      jac1d1r_abft(in, out, w0, w1, w2, T, N, zT, zI, check);
      #else
      // run vanilla protypical kernel
      jac1d1r(in, out, w0, w1, w2, T, N);
      #endif
      stop_timer(elapsed_time2);
    } else if (thread_num == 1) {
      #ifdef ERR_INJECTION
      // error injection
      inject(out, N, elapsed_time1/3);
      #endif
    }
  }

  #ifdef KERNEL_WITH_ABFT
  for (int tt=0; tt<zT; tt++)
    for (int ti=1; ti<zI-1; ti++) {
      float rel_error = 100 * fabsf((checksum(tt,ti) - invariant(tt,ti)) / checksum(tt,ti));
      #define THRESHOLD 0.0001 
      if (rel_error > THRESHOLD) {  
        printf("[tt,ti]=[%d,%d] -> relative error (%%): %E (%%)\n", tt, ti, rel_error);
        //printf("checksum:  %E\n", checksum(tt,ti));
        //printf("invariant: %E\n", invariant(tt,ti));
      }
    }
  #endif

  printf("%f sec\n", elapsed_time2);
 
  #ifdef CHECK
  // sanity checking
  jac1d1r(in, out_check, w0, w1, w2, T, N);
  for (int t=0; t<2; t++)
  for (int i=0; i<=N; i++) {
    float difference = fabsf(out[(t)*(N+1)+(i)] - out_check[(t)*(N+1)+(i)]);
    if (difference > 0.00001)
     printf("%d,%d --> %f,%f\n", t, i, out[(t)*(N+1)+(i)], out_check[(t)*(N+1)+(i)]);
  }
  #endif 

}
