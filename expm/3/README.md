# Overhead of ABFT control flow and additional computations

This is all for the 1D 3-point jacobi example.
Let `W` be the window size (along i) and `S` be the depth (along t) of the checksum/invariant region.

To generate the final C code for jac1d1r with W=32 and S=5, for example, do the following:
1. edit the `templates/_template.iscc` to replace `__W__` and `__S__` with 32 and 5 respectively
2. call ISL (via some tool, I use iscc here) to generate the loops
3. paste the contents into the `templates/_template.c` file between the `ISCC codegen here` comments
4. edit that C file to set the macro defintion values for W and S
5. compile the C file

The `lcc` script just automates these manual commands above, i.e., just do:
```
$ ./lcc 50 5
created src/W.50.S.5.c
gcc -O3 -o bin/jac.50.5 src/rand.c src/W.50.S.5.c -fopenmp -lcrypto -lm
gcc -O3 -o bin/jac.50.5.abft src/rand.c src/W.50.S.5.c -fopenmp -lcrypto -lm -DKERNEL_WITH_ABFT
gcc -O3 -o bin/jac.50.5.abft.check src/rand.c src/W.50.S.5.c -fopenmp -lcrypto -lm -DKERNEL_WITH_ABFT -DCHECK
gcc -O3 -o bin/jac.50.5.abft.inject src/rand.c src/W.50.S.5.c -fopenmp -lcrypto -lm -DKERNEL_WITH_ABFT -DERR_INJECTION
```

This creates the relevant source and binary files:
```
$ ls -l src/ bin/
bin/:
total 184
-rwxr-xr-x 1 lnarmour taran 41464 Mar  2 16:56 jac.50.5
-rwxr-xr-x 1 lnarmour taran 41464 Mar  2 16:56 jac.50.5.abft
-rwxr-xr-x 1 lnarmour taran 45560 Mar  2 16:56 jac.50.5.abft.check
-rwxr-xr-x 1 lnarmour taran 45616 Mar  2 16:56 jac.50.5.abft.inject

src/:
total 36
-rw-r--r-- 1 lnarmour taran   344 Mar  2 15:04 Makefile
-rw-r--r-- 1 lnarmour taran  1183 Mar  2 14:06 rand.c
-rw-r--r-- 1 lnarmour taran 18529 Mar  2 16:56 W.50.S.5.c
-rw-r--r-- 1 lnarmour taran  4299 Mar  2 16:56 W.50.S.5.iscc
```

The `jac.50.5` version just runs the original stencil kernel with no ABFT control flow.
The `jac.50.5.abft` version has all of the checksum/invariant patch control flow (~276 lines).
The difference between the two represents the overhead.

As written here and compiled with gcc `-O3`, the overhead is too large (~16x) for this to be worth it:
```
$ ./bin/jac.50.5 1000 1000000
0.271857 sec
$ ./bin/jac.50.5.abft 1000 1000000
4.033722 sec
```

Even if we turn off all gcc optimizations, the runtime differs by too much (~4x):
```
$ ./bin/jac.50.5 1000 1000000
4.963387 sec
$ ./bin/jac.50.5.abft 1000 1000000
17.007620 sec
```
