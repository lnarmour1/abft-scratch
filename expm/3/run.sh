#!/bin/bash

rm -rf .lcc.log

export OMP_NUM_THREADS=2
export GOMP_CPU_AFFINITY=0,1

function run {
  T=$1
  N=$2
  W=$3
  S=$4
 
  ./lcc $W $S >> .lcc.log
 
  cmd="./bin/jac.$W.$S $T $N"
  echo "jac1d1r, no abft, no error injection ($cmd)"
  for r in {1..5}
  do 
    eval $cmd 
  done
  echo
  
  cmd="./bin/jac.$W.$S.abft $T $N"
  echo "jac1d1r, with abft, no error injection ($cmd)"
  for r in {1..5}
  do 
    eval $cmd 
  done
  echo
  
  cmd="./bin/jac.$W.$S.abft.inject $T $N"
  echo "jac1d1r, with abft, with error injection ($cmd)"
  for r in {1..5}
  do 
    eval $cmd 
  done
  echo
}

Ws=(32 64)
Ss=(5 10)
for W in ${Ws[@]}; do
for S in ${Ss[@]}; do
  run 10000 2000000 $W $S
done
done


Ws=(100 200 300 400)
Ss=(5 10 25)
for W in ${Ws[@]}; do
for S in ${Ss[@]}; do
  run 10000 2000000 $W $S
done
done


Ws=(1000 5000)
Ss=(5 200)
for W in ${Ws[@]}; do
for S in ${Ss[@]}; do
  run 10000 2000000 $W $S
done
done

