#include<openssl/rand.h>
#include <stdio.h>

// https://stackoverflow.com/questions/822323/how-to-generate-a-random-int-in-c/39475626

/* Random integer in [0, limit) */
unsigned int lrand_uint(unsigned int limit) {
    unsigned int val;
    unsigned int max_val = 1 << (8*sizeof(unsigned int)-1);
    do {
      if (!RAND_bytes((unsigned char *)&val, sizeof(unsigned int))) {
        fprintf(stderr, "Can't get random bytes!\n");
        exit(1);
      }
    } while (val < max_val % limit);
    
    return val % limit;
}

/* Random float in [0.0, limit) */
float lrand_float(float limit) {
    unsigned int val;
    unsigned int one = 1;
    if (!RAND_bytes((unsigned char *)&val, sizeof(unsigned int))) {
      fprintf(stderr, "Can't get random bytes!\n");
      exit(1);
    }
    /* 24 bits / 2**24 */
    return (val >> 8) * (limit / (one << 24));
}

/* Random double in [0.0, limit) */
double lrand_double(double limit) {
    uint64_t val;
    uint64_t one = 1;
    if (!RAND_bytes((unsigned char *)&val, sizeof(uint64_t))) {
      fprintf(stderr, "Can't get random bytes!\n");
      exit(1);
    }
    /* 53 bits / 2**53 */
    return (val >> 11) * (limit / (one << 53));
}
