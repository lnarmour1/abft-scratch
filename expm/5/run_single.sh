#!/bin/bash

if [[ -z "$1" || -z "$2" ]]; then
  echo "usage: $0 S W [abft|abft.inject]"
  exit 1;
fi

S=$1
W=$2
ex=$3

T=10000
N=100000

function run {
  S=$1
  W=$2
  if [[ -n "$3" ]]; then
    ex=.$3
  fi
  binary=./bin/jac.$S.$W$ex
  if [[ ! -f $binary ]]; then
    ./lcc $S $W > /dev/null
  fi
  echo "$3,$S,$W,$T,$N,$($binary $T $N 2>/dev/null)"
}

run $S $W $ex
