#!/bin/bash

L=$1

if [[ -z "$L" ]]; then
  echo "usage: $0 L ($(for l in `python3 main.py | cut -d',' -f1 | uniq`; do printf "$l "; done;))" | sed 's~ )~)~'
  exit 1;
fi

echo 'overhead (%):'
cat digest-overhead.log | head -n2
cat digest-overhead.log | grep "^$L "
echo
echo 'error detection rate (%):'
cat digest-err.log | head -n2
cat digest-err.log | grep "^$L "
