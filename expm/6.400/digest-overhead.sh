#!/bin/bash

if [[ -z "$1" ]]; then
  echo "usage: $0 threshold"
  exit 1
fi

THRESHOLD=$1

function dump {
  machine=$1
  for LSW in `python3 main.py`
  do 
    SW=`echo $LSW | sed 's~^[^,]*,\(.*\)~\1~'`
    cmd="cat all-$machine.$THRESHOLD.log | grep ',$SW,' | cut -d',' -f14"
    overhead=0
    for o in `eval $cmd`
    do
      overhead=`bc -l <<< 'scale=6; '$overhead'+'$o''`
    done
    overhead=`bc -l <<< 'scale=2; '$overhead'/51'`
    echo $overhead
  done
}

pcmd="paste -d',' <(echo 'L,S,W,|'; echo '-,-,-,-'; python3 main.py | sed 's~\(.*\)~\1,|~')"
bit=31
for m in `cat fish-machines.txt | grep -v '^#'`
do
  pcmd+=" <(echo $bit; echo '-'; dump $m)"
  bit=$((bit-1))
done

if [[ -z "$CSV" ]]; then
  eval $pcmd | column -t -s,
else
  eval $pcmd
fi

