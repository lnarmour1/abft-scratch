#!/bin/bash

if [[ -z "$1" ]]; then
  echo "usage: $0 threshold"
  exit 1
fi

threshold=$1

total=1
for S in {4..40..2}; do for r in {0..50}; do total=$((total+1)); done; done;

function all {
  bit=31
  for m in `cat fish-machines.txt | grep -v '^#'`
  do
    lines=`cat all-$m.$threshold.log | wc -l`
    echo "$bit,$m.$threshold,->,$lines/$total (`bc -l <<< 'scale=2; 100*'$lines'/'$total''`%%)"
    bit=$((bit-1))
  done
}

all | column -t -s,
