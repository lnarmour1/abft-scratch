#!/bin/bash

if [[ -z "$1" ]]; then
  echo "usage: $0 DIRECTORY"
  exit 1
fi

dir=`echo $1 | sed 's~\(.*\)/~\1~'`

mkdir -p $dir

functions=(step flat)
bitss=(0.31 23.31 0.22 13.22)
rhos=(0.1 1 10 100 1000)

for func in ${functions[@]}
do
  for bits in ${bitss[@]}
  do
    for rho in ${rhos[@]}
    do
      for N in `python logspace.py 16`
      do 
        echo $N,$(./swal.$func.$bits $N 1 1000) >> data/$func.bits.$bits.rho.$rho.txt
      done
    done
  done
done
