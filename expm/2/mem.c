#include <iostream>
#include <iterator>
#include <random>

extern "C" float* step(int, int, float*);
extern "C" float* flat(int, int, float*);

const float mean = 0.0;
const float stddev = 0.1;
std::default_random_engine generator;
std::normal_distribution<float> dist(mean, stddev);

float* step(int N, int magnitude, float *data) {
  for (int i=0; i<N/2; i++) 
    data[i] = 0.0 + dist(generator);
  for (int i=N/2; i<N; i++) 
    data[i] = magnitude + dist(generator);
  return data;
}

float* flat(int N, int magnitude, float *data) {
  for (int i=0; i<N; i++) 
    data[i] = magnitude + dist(generator);
  return data;
}
