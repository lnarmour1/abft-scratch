from matplotlib import pyplot as plt
import numpy as np
import os
import sys
import re

if len(sys.argv) < 2:
    print('{} FILE0 [FILE1 [...]]'.format(sys.argv[0]))
    sys.exit(1)

Fs = sys.argv[1:]

data = {}

for F in Fs:
    s = F.split('/')[-1]
    func, bits, limit = [f if '.' not in f else float(f) for f in re.sub('(.*).bits.(.*).rho.(.*).txt', r'\1 \2 \3', s).split(' ')]

    limit = float(limit)
    with open(F, 'r') as f:
        raw = [line.replace('\n', '') for line in f.readlines()]
    ret = [] 
    for line in raw:
        N, success_rate = line.split(',')
        N, success_rate = int(N), float(success_rate)
        ret.append([N, success_rate])
    data['{}-{}-{}'.format(func,bits,limit)] = np.array(ret)

L = {'0.22': 'full mantissa', '0.31': 'any bit', '13.22': 'partial mantissa', '23.31': 'exponent+sign'}

limits = []
funcs = []
for k in data:
    func, bits, limit = k.split('-')
    limits.append(limit)
    funcs.append(func)
    #print(data[k])
    X = data[k][:,0]
    Y = data[k][:,1]
    plt.scatter(X, Y, label='{} ({})'.format(L[bits], bits))

if len(set(limits)) > 1:
    raise Exception('more than one limit range, this plot will not be easily readable')
if len(set(funcs)) > 1:
    raise Exception('more than one function, this plot will not be easily readable')

plt.title('Error detection of random bit flip\namong N floats in range [0,{}) for {} function'.format(limit, func))
plt.xlabel('number of values (N)')
plt.ylabel('success rate')
plt.xscale('log')
plt.legend()
plt.savefig('imgs/{}.N.{}.pdf'.format(func, limit))
#plt.show()

