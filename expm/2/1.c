#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>

unsigned int lrand_uint(unsigned int limit);
float lrand_float(float limit);
double lrand_double(double limit);
float* step(int N, int magnitude, float *in);
float* flat(int N, int magnitude, float *in);

void usage(char *argv[]) {
  printf("%s N limit num_runs\n", argv[0]);
}

void flip(float *val, int bit_num) {
  int *bits;
  bits = (int*)val;
  *bits ^= 1 << bit_num;
}

float sum(int l, int r, float *arr) {
  if (r-l <= 10) {
    float ret = 0.0;
    for (int i=l; i<r; i++) 
      ret += arr[i];
    return ret; 
  }
  int m = (l+r)/2;
  float l_sum;
  float r_sum;
  l_sum = sum(l, m, arr);
  r_sum = sum(m, r, arr);

  return l_sum + r_sum;
}

int run(int N, int limit, float *in) {

  #ifdef F_STEP  
  in = step(N, limit, in);
  #elif F_FLAT
  in = flat(N, limit, in);
  #elif defined F_DIRAC

  #elif F_SMOOTH
  
  #else
  float *error_out = 0;
  error_out[0] = 1; // this will throw a runtime error
  #endif
  
  float checksum_f = 0.0; 
  float checksum_b = 0.0; 
  float checksum_wflip = 0.0;

  // checksum forward
  for (int i=0; i<N; i++) {
    checksum_f += in[i];
  }
  // checksum different order
  checksum_b = sum(0, N, in);
   
  // flip bit in j'th entry
  long j = lrand_uint(N);
  #ifdef EXP_BITS
  long bit_num = 23 + lrand_uint(8);
  #elif defined MANTISSA_BITS
  long bit_num = lrand_uint(23);
  #elif defined SIG_MANTISSA_BITS
  long bit_num = 13 + lrand_uint(10);
  #else
  long bit_num = lrand_uint(32);
  #endif
  float old = in[j];
  flip(&in[j], bit_num); 

  // checksum forward again
  for (int i=0; i<N; i++) {
    checksum_wflip += in[i];
  }

  float rel_err_wflip = fabs((checksum_f - checksum_wflip) / checksum_f);
  float rel_err_fb = fabs((checksum_f - checksum_b) / checksum_f);
  float bump = rel_err_wflip / rel_err_fb;

  int detected;
  // associativity diff is 0.000E+00
  if (rel_err_fb == 0) {
    detected = rel_err_wflip > 0.00001 ? 1 : 0;
  } else {
    detected = bump > 10;
  }

  #ifdef VERBOSE
  printf("checksum_f      = %f\n", checksum_f);
  printf("checksum_b      = %f (%E)\n", checksum_b, rel_err_fb);
  printf("checksum_wflip  = %f (%E)\n", checksum_wflip, rel_err_wflip);
  printf(detected==1 ? "\u2705\n" : "\u274C\n");
  #endif
  return detected;
}

int main(int argc, char* argv[]) {

  if (argc != 4) {
    usage(argv);
    return 1;
  }

  int N = atol(argv[1]);
  int limit = atol(argv[2]);
  int runs = atol(argv[3]);

  float *in = (float*)malloc(sizeof(float) * N);
  int successful_detections = 0;  
 
  for (int i=0; i<runs; i++) {
    srand(i);
    successful_detections += run(N, limit, in);
  }
  printf("%f\n", ((float)successful_detections)/runs);
}

