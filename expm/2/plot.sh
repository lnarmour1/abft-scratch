#!/bin/bash

if [[ -z "$1" || -z "$2" ]]; then
  echo "usage: $0 DIRECTORY FUNC"
  exit 1
fi

dir=`echo $1 | sed 's~\(.*\)/~\1~'`
func=$2

function limits { 
  for f in ${dir}/${func}*.txt
  do 
    echo $f | sed 's~.*rho.\(.*\).txt~\1~' 
  done 
} 

mkdir -p imgs

for l in `limits | sort | uniq`
do
  python plot.py ${dir}/${func}*rho.${l}.txt
done

for f in imgs/${func}*.pdf
do
  xdg-open $f
done
