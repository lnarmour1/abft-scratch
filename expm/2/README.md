# Sensitivity

The ability to reliably detect errors is sensitive to the number of values under consideration.
We're much more likely to be able to observe a difference in the sum of 2 numbers vs. 1000 for example.
This means that the checksum and the invariants must be small enough.

These plots make me think that this doesn't really work for the LARGE general case.
What is the minimum success rate we can tolerate?
This is dependent on the application itself, among other things.
There are scenarios where an error can cause the outcome of the application to become corrupt but that error cannot be detected in the checksum.

Is there another way to encode the checksum, like with a shasum or something like this that is not sensitive to the number of values or the relative magnitude of values?
