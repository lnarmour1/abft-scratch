from matplotlib import pyplot as plt
import numpy as np
import os
import sys

if len(sys.argv) != 3:
    print('{} FILE THRESHOLD'.format(sys.argv[0]))
    sys.exit(1)

F = sys.argv[1]
THRESHOLD = float(sys.argv[2])

with open(F, 'r') as f:
    raw = [line.replace('\n', '') for line in f.readlines()]

params = []
data = []
for line in raw:
    N, bit_num, limit, ch1, ch2, rel_err = line.split(',')
    params.append([int(N), int(bit_num), int(limit)])
    err = float(rel_err) * 100
    err = min(err, THRESHOLD)
    data.append([float(ch1), float(ch2), float(err)])

params = np.array(params)
data = np.array(data)

fig, axs = plt.subplots(4, 8, sharex='all', sharey='all')

bit_num = 0
for i in reversed(range(4)):
    for j in reversed(range(8)):
        X = params[np.where(params[:,1]==bit_num), 0]
        Y = data[np.where(params[:,1]==bit_num) ,-1]
        axs[i,j].scatter(X, Y, label=bit_num)
        axs[i,j].set_xscale('log')
#        axs[i,j].set_yscale('log')
        axs[i,j].set(title='bit {}'.format(bit_num))
        bit_num += 1

for i in range(4):
    axs[i,0].set_ylabel('difference (%)')
for j in range(8):
    axs[3,j].set_xlabel('N')


fig.suptitle('min(difference, {}), vals in [0,{})'.format(THRESHOLD, params[0,2]))
plt.show()
