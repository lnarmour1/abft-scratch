import numpy as np
import sys
print(' '.join([str(n) for n in np.logspace(1, int(sys.argv[-1]), dtype=int, base=2)]))
