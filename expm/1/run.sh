#!/bin/bash

LIMIT=1
OUT=out.txt
rm -rf out.txt

# ' '.join([str(n) for n in numpy.logspace(1, 17, dtype=int, base=2)])
Ns=`python logspace.py 20`
Ns=`for i in {2..32}; do printf "$i "; done;`

for bit_num in {0..31};
do
  for N in ${Ns[@]}
  do
    for run in {1..10}
    do
      ./swal $N $bit_num $LIMIT >> $OUT
    done
  done
done
