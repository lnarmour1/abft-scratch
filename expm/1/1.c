#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>

unsigned int lrand_uint(unsigned int limit);
float lrand_float(float limit);
double lrand_double(double limit);

void usage(char *argv[]) {
  printf("%s N bit_num limit\n", argv[0]);
}

void flip(float *val, int bit_num) {
  int *bits;
  bits = (int*)val;
  *bits ^= 1 << bit_num;
}

int main(int argc, char* argv[]) {

  if (argc != 4) {
    usage(argv);
    return 1;
  }

  int N = atol(argv[1]);
  int bit_num = atol(argv[2]);
  int limit = atol(argv[3]);

  float *in = (float*)malloc(sizeof(float)*(N));

  // initialize random values from [0,limit)
  for (int i=0; i<N; i++) {
    in[i] = lrand_float(limit);
  }
  
  float checksum1 = 0.0; 
  float checksum2 = 0.0;

  // checksum 1
  for (int i=0; i<N; i++) {
    checksum1 += in[i];
  }

  // flip bit in j'th entry
  long j = lrand_uint(N);
  //printf("j = %d\n", j);
  flip(&in[j], bit_num); 

  // checksum 2 
  for (int i=0; i<N; i++) {
    checksum2 += in[i];
  }

  float rel_err = fabs((checksum1 - checksum2) / checksum1);

  //printf("checksum1 = %f (%E)\n", checksum1, checksum1);
  //printf("checksum2 = %f (%E)\n", checksum2, checksum2);
  //printf("rel error = %f (%E)\n", rel_err, rel_err); 
  printf("%d,%d,%d,%E,%E,%E\n", N, bit_num, limit, checksum1, checksum2, rel_err); 
}
