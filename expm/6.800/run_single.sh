#!/bin/bash

if [[ -z "$1" || -z "$2" || -z "$3" || -z "$4" ]]; then
  echo "usage: $0 S W bit threshold [abft|abft.inject]"
  exit 1;
fi

S=$1
W=$2
bit=$3
threshold=$4
ex=$5

T=10000
N=100000

function run {
  S=$1
  W=$2
  bit=$3
  if [[ -n "$4" ]]; then
    binary=./bin/jac.$S.$W.$4
    ex=$4
  else
    binary=./bin/jac.$S.$W
    ex=baseline
  fi
  if [[ ! -f $binary ]]; then
    ./lcc $S $W > /dev/null
  fi
  echo "$ex,$S,$W,$T,$N,$($binary $T $N $bit $threshold)" # 2>/dev/null)"
}

run $S $W $bit $ex
