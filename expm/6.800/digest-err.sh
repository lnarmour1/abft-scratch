#!/bin/bash

if [[ -z "$1" ]]; then
  echo "usage: $0 threshold"
  exit 1
fi

THRESHOLD=$1

function dump {
  machine=$1
  for LSW in `python3 main.py`
  do 
    SW=`echo $LSW | sed 's~^[^,]*,\(.*\)~\1~'`
    cmd="cat all-$machine.$THRESHOLD.log | grep ",$SW," | cut -d',' -f11"
    paste -d'/' <(eval $cmd | grep -v 0 | wc -l) <(eval $cmd | wc -l) | sed "s~\(.*\)~$SW,\1~"
  done
}

function all {
  machine=$1
  for line in `dump $machine`
  do
    num=`echo $line | cut -d',' -f3 | cut -d'/' -f1`
    err_rate=`bc -l <<< 'scale=2; 100*'$num'/51'`
    echo $err_rate
  done
}

pcmd="paste -d',' <(echo 'L,S,W,|'; echo '-,-,-,-'; python3 main.py | sed 's~\(.*\)~\1,|~')"
bit=31
for m in `cat fish-machines.txt | grep -v '^#'`
do
  pcmd+=" <(echo $bit; echo '-'; all $m)"
  bit=$((bit-1))
done

if [[ -z "$CSV" ]]; then
  eval $pcmd | column -t -s,
else
  eval $pcmd
fi
