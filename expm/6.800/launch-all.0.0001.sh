#!/bin/bash


for m in `cat fish-machines.txt | grep -v '^#'`
do
  echo $m
  ssh $m tmux new-session -d -s abft 'bash ~/git/abft-scratch/expm/6.800/script-gen/run_all_'$m'.0.0001.sh'
done
