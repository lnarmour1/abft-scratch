#!/bin/bash


function run_them {
  SUFFIX=$1  

  ./lcc 1000 100000
  for r in {1..10}; do ./bin/jac.1000.100000$SUFFIX 1000 100000; done;

  ./lcc 500 50000
  for r in {1..10}; do ./bin/jac.500.50000$SUFFIX 1000 100000; done;
  
  ./lcc 10 100
  for r in {1..10}; do ./bin/jac.10.100$SUFFIX 1000 100000; done;
  
  ./lcc 5 25
  for r in {1..10}; do ./bin/jac.5.25$SUFFIX 1000 100000; done;

  echo
  echo "baseline (no-abft)"
  ./bin/jac.5.50 1000 100000
}

run_them .abft.inject
