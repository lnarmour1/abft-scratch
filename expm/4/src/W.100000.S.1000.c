#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <unistd.h>
#include<sys/time.h>
#include<omp.h>

struct timeval ttime;
double elapsed_time1;
double elapsed_time2;
#define start_timer(t) gettimeofday(&ttime, NULL); t = (((double) ttime.tv_sec) + ((double) ttime.tv_usec)/1000000)
#define stop_timer(t) gettimeofday(&ttime, NULL); t = (((double) ttime.tv_sec) + ((double) ttime.tv_usec)/1000000) - t

#define max(x, y)   ((x)>(y) ? (x) : (y))
#define min(x, y)   ((x)>(y) ? (y) : (x))
#define floord(n,d) (int)floor(((float)(n))/((float)(d)))

#define output(t,i) out[(t)%2][i]
#define input(i) in[i]

#define W 100000
#define S 1000

unsigned int lrand_uint(unsigned int limit);
float lrand_float(float limit);
double lrand_double(double limit);

// N+1 data grid over T timesteps
void jac1d1r(float *in, float **out, float w0, float w1, float w2, long T, long N, int zt) {
  long t, i;
  for (i=0; i<=N; i++)
    output(0,i) = zt==0 ? input(i) : output(0,i);
  for (t=1; t<=T; t++) {
    output(t,0) = output(t-1,0);
    for (i=1; i<N; i++)
      output(t,i) = w0*output(t-1,i-1) + w1*output(t-1,i) + w2*output(t-1,i+1);
    output(t,N) = output(t-1,N);
  }
}

void jac1d1r_wrapper(float *in, float **out, float w0, float w1, float w2, long T, long N, int zT) {
  int zt = 0;
  while (zt<zT) {
    jac1d1r(in, out, w0, w1, w2, S, N, zt);
    zt++;
  }
}


void inject(float **out, int N, double time_window) {
  int j = lrand_uint(N);
  float *val = &out[0][j];
  int bit_to_flip = lrand_uint(32);

  // it takes roughly time_window time to process all T timesteps of the stencil
  // sleep for X secs for some random 
  double duration = lrand_double(1.0) * time_window * 1000000; //microseconds
  usleep(duration);

  int *bits;
  bits = (int*)val;

  //float o1 = *bits;
  *bits ^= 1 << bit_to_flip;
  *(bits+N) ^= 1 << bit_to_flip;
  //float n1 = *bits;
  //printf("out[%d] flipped bit %d (%f -> %f)\n", j, bit_to_flip, o1, n1); 
  printf("out[%d] flipped bit %d\n", j, bit_to_flip); 
}

void jac1d1r_abft(float *in, float **out, float w0, float w1, float w2, int T, int N, int zt, int zI, float *check) {
  float alpha = w0 + w1 + w2;

  float alpha_to_the[S+1];
  alpha_to_the[0] = 1.0;
  for (int i=1; i<=S; i++) {
    alpha_to_the[i] = alpha_to_the[i-1] * alpha;
  }

  #define S0(t,i) output(t,i) = zt == 0 ? input(i) : output(t,i)
  #define S1(t,i) output(t,i) = output(t-1,i)
  #define S2(t,i) output(t,i) = w0*output(t-1,i-1) + w1*output(t-1,i) + w2*output(t-1,i+1)
  #define S3(t,i) output(t,i) = output(t-1,i)

  #define checksum(ti) check[0*zI+(ti)]
  #define invariant(ti) check[1*zI+(ti)]
  #define inv_K(ti) check[2*zI+(ti)]
  #define inv_B1(ti) check[3*zI+(ti)]
  #define inv_B2(ti) check[4*zI+(ti)]
  #define inv_R1(ti) check[5*zI+(ti)]
  #define inv_R2(ti) check[6*zI+(ti)]
  
  #define C(t,i,p,ti) checksum(ti) += output(t,i)
  #define K(t,i,p,ti) inv_K(ti) += output(t,i)
  #define B1(t,i,p,ti) inv_B1(ti) += alpha_to_the[p-1] * output(t,i)
  #define B2(t,i,p,ti) inv_B2(ti) += alpha_to_the[p-1] * output(t,i)
  #define R1(t,i,p,ti) inv_R1(ti) += alpha_to_the[p-1] * output(t,i)
  #define R2(t,i,p,ti) inv_R2(ti) += alpha_to_the[p-1] * output(t,i)

  // ISCC codegen below
  if (zI >= 1) {
    for (int c3 = 0; c3 <= N; c3 += 1)
      S0(0, c3);
    if (N <= -1) {
      for (int c1 = 1; c1 <= T; c1 += 1) {
        S3(c1, N);
        S1(c1, 0);
      }
    } else {
      for (int c1 = 0; c1 <= T; c1 += 1) {
        if (c1 >= 1) {
          S1(c1, 0);
          for (int c3 = 1; c3 < N; c3 += 1)
            S2(c1, c3);
          S3(c1, N);
        }
        if (N >= c1 + 98001 && N + c1 <= 98997) {
          if (c1 == 0)
            for (int c5 = 0; c5 <= 98000; c5 += 1)
              K(0, c5, 1000, 0);
          B2(c1, c1 + 98001, -c1 + 1000, 0);
          if (N >= c1 + 98002)
            R2(c1, c1 + 98002, -c1 + 1000, 0);
        } else if (N + c1 >= 98998 && c1 <= 999) {
          if (zI >= 2)
            for (int c3 = -c1 + 98998; c3 <= min(min(N, c1 + 98000), -c1 + 98999); c3 += 1) {
              if (c1 + c3 == 98999) {
                B1(c1, -c1 + 98999, -c1 + 1000, 1);
              } else {
                R1(c1, -c1 + 98998, -c1 + 1000, 1);
              }
            }
          if (c1 == 0) {
            for (int c3 = 0; c3 <= min(zI, N / 99000); c3 += 1) {
              if ((c3 + 1) % 99000 == 0) {
                B1(0, c3, 1000, (c3 + 1) / 99000);
              } else if ((c3 + 2) % 99000 == 0) {
                R1(0, c3, 1000, (c3 + 2) / 99000);
              }
              if (c3 >= 1 && zI >= c3 + 1) {
                for (int c5 = 99000 * c3; c5 <= min(N, 99000 * c3 + 98000); c5 += 1)
                  K(0, c5, 1000, c3);
              } else if (c3 == zI) {
                for (int c5 = 99000 * zI; c5 <= min(N, 99000 * zI + 98000); c5 += 1)
                  K(0, c5, 1000, zI);
              } else {
                for (int c5 = 0; c5 <= 98000; c5 += 1)
                  K(0, c5, 1000, 0);
              }
              if ((c3 + 998) % 99000 == 0) {
                R2(0, c3, 1000, (c3 - 98002) / 99000);
              } else if ((c3 + 999) % 99000 == 0) {
                B2(0, c3, 1000, (c3 - 98001) / 99000);
              }
            }
            for (int c3 = max(98001, N / 99000 + 1); c3 <= min(N, zI); c3 += 1) {
              if ((c3 + 1) % 99000 == 0) {
                B1(0, c3, 1000, (c3 + 1) / 99000);
              } else if ((c3 + 2) % 99000 == 0) {
                R1(0, c3, 1000, (c3 + 2) / 99000);
              } else if ((c3 + 998) % 99000 == 0) {
                R2(0, c3, 1000, (c3 - 98002) / 99000);
              } else if ((c3 + 999) % 99000 == 0) {
                B2(0, c3, 1000, (c3 - 98001) / 99000);
              }
            }
            for (int c3 = max(98001, zI + 1); c3 <= min(N, 99000 * zI - 998); c3 += 1) {
              if ((c3 + 1) % 99000 == 0) {
                B1(0, c3, 1000, (c3 + 1) / 99000);
              } else if ((c3 + 2) % 99000 == 0) {
                R1(0, c3, 1000, (c3 + 2) / 99000);
              } else if ((c3 + 998) % 99000 == 0) {
                R2(0, c3, 1000, (c3 - 98002) / 99000);
              } else if ((c3 + 999) % 99000 == 0) {
                B2(0, c3, 1000, (c3 - 98001) / 99000);
              }
            }
          } else {
            for (int c3 = c1 + 98001; c3 <= min(min(N, 99000 * zI + c1 - 998), 99000 * zI - c1 - 3); c3 += 1) {
              if ((c1 + c3 + 1) % 99000 == 0) {
                B1(c1, c3, -c1 + 1000, (c1 + c3 + 1) / 99000);
              } else {
                if ((c1 + c3 + 2) % 99000 == 0)
                  R1(c1, c3, -c1 + 1000, (c1 + c3 + 2) / 99000);
                if ((c1 - c3 - 998) % 99000 == 0)
                  R2(c1, c3, -c1 + 1000, (-c1 + c3 - 98002) / 99000);
              }
              if ((c1 - c3 - 999) % 99000 == 0)
                B2(c1, c3, -c1 + 1000, (-c1 + c3 - 98001) / 99000);
            }
          }
          for (int c3 = 99000 * zI - c1 - 2; c3 <= min(N, 99000 * zI - c1 - 1); c3 += 1) {
            if (c1 + c3 + 1 == 99000 * zI) {
              B1(c1, 99000 * zI - c1 - 1, -c1 + 1000, zI);
              if (c1 == 499)
                B2(499, 99000 * zI - 500, 501, zI - 1);
            } else {
              R1(c1, 99000 * zI - c1 - 2, -c1 + 1000, zI);
              if (c1 == 498)
                R2(498, 99000 * zI - 500, 502, zI - 1);
            }
          }
          for (int c3 = max(99000 * zI + c1 - 999, 99000 * zI - c1); c3 <= min(N, 99000 * zI + c1 - 998); c3 += 1) {
            if (c3 + 998 == 99000 * zI + c1) {
              R2(c1, 99000 * zI + c1 - 998, -c1 + 1000, zI - 1);
            } else {
              B2(c1, 99000 * zI + c1 - 999, -c1 + 1000, zI - 1);
            }
          }
          for (int c3 = 99000 * zI + c1 + 98001; c3 <= min(N, 99000 * zI + c1 + 98002); c3 += 1) {
            if (c3 == 99000 * zI + c1 + 98002) {
              R2(c1, 99000 * zI + c1 + 98002, -c1 + 1000, zI);
            } else {
              B2(c1, 99000 * zI + c1 + 98001, -c1 + 1000, zI);
            }
          }
        } else if (c1 == 1000) {
          for (int c3 = 0; c3 <= min(zI, (N + 1000) / 99000); c3 += 1) {
            if (c3 >= 1) {
              for (int c5 = 99000 * c3 - 1000; c5 <= min(N, 99000 * c3 + 99000); c5 += 1)
                C(1000, c5, 0, c3);
            } else {
              for (int c5 = 0; c5 <= min(99000, N); c5 += 1)
                C(1000, c5, 0, 0);
            }
          }
        } else if (N <= 98000 && c1 == 0) {
          for (int c5 = 0; c5 <= N; c5 += 1)
            K(0, c5, 1000, 0);
        }
      }
    }
  }
  // ISCC codegen above

  // multiple by coeffs
  for (int ti=0; ti<zI; ti++) {
    invariant(ti) = (w0)*inv_R1(ti) + (w0+w1)*inv_B1(ti) + (alpha_to_the[S])*inv_K(ti) + (w1+w2)*inv_B2(ti) + (w2)*inv_R2(ti);
  }
}

void jac1d1r_abft_wrapper(float *in, float **out, float w0, float w1, float w2, int T, int N, int zT, int zI, float *check) {

  int zt = 0;
  while (zt<zT) {
    // zero out checksum & invariant values
    for (int i=0; i<7*zI; i++)
      check[i] = 0.0;

    jac1d1r_abft(in, out, w0, w1, w2, S, N, zt, zI, check);

    for (int ti=1; ti<zI-1; ti++) {
      float rel_error = 100 * fabsf((checksum(ti) - invariant(ti)) / checksum(ti));
      #define THRESHOLD 0.0001 
      if (rel_error > THRESHOLD) {
        printf("[ti]=[%d] -> relative error (%%): %E (%%)\n", ti, rel_error);
        // reset to last checkpoint and redo
      }
    }
    
    // there were no errors, update checkpoint
    zt++;
  }
}

int main(int argc, char* argv[]) {
  
  if (argc < 3) {
    printf("usage: %s T N\n", argv[0]);
    return 1;
  }

  long T = atol(argv[1]);
  long N = atol(argv[2]);

  if (!(0<T && 0<N)) {
    printf("unsupported parameter values, must be positive.\n");
    return 1;
  }    
  if (!(W<=N)) {
    printf("unsupported parameter values, expecting N>=%d\n", W);
    return 1;
  }    
  if (!(T>=S)) {
    printf("unsupported parameter values, expecting T>=%d\n", S);
    return 1;
  }    
  if (!(T%S==0)) {
    printf("unsupported parameter values, expecting T to be a multiple of %d\n", S);
    return 1;
  }    

  float w0 = 0.3333;
  float w1 = 0.3333;
  float w2 = 1 - w0 - w1;

  float *in = (float*)malloc((N+1)*sizeof(float));
  float *blob = (float*)malloc((N+1)*3*sizeof(float));
  float *out[3] = { &blob[0], &blob[N], &blob[2*N] };
  #ifdef CHECK
  blob = (float*)malloc((N+1)*2*sizeof(float));
  float *out_check[2] = { &blob[0], &blob[N] };
  #endif

  // initialize random input
  for (int i=0; i<=N; i++) {
    in[i] = lrand_float(10);
  }

  // allocate memory for checksums and initialize to zero
  int zI = (N / (W-S));
  int zT = (T / S);
  float *check = (float*)malloc(7*zI*sizeof(float));

  #ifdef ERR_INJECTION
  // get average running time
  start_timer(elapsed_time1);
  for (int r=0; r<3; r++)
    jac1d1r_abft_wrapper(in, out, w0, w1, w2, T, N, zT, zI, check);
  stop_timer(elapsed_time1);

  // reset checksums
  for (int i=0; i<7*zI; i++)
    check[i] = 0.0;
  #endif

  #pragma omp parallel 
  {
    int thread_num = omp_get_thread_num();
    if (thread_num == 0) {
      start_timer(elapsed_time2);
      #ifdef KERNEL_WITH_ABFT
      // run kernel + checksum
      jac1d1r_abft_wrapper(in, out, w0, w1, w2, T, N, zT, zI, check);
      #else
      // run vanilla protypical kernel
      jac1d1r_wrapper(in, out, w0, w1, w2, T, N, zT);
      #endif
      stop_timer(elapsed_time2);
    } else if (thread_num == 1) {
      #ifdef ERR_INJECTION
      // error injection
      inject(out, N, elapsed_time1/3);
      #endif
    }
  }


  printf("%f sec\n", elapsed_time2);
 
  #ifdef CHECK
  // sanity checking
  jac1d1r(in, out_check, w0, w1, w2, T, N, 0);
  for (int t=0; t<2; t++)
  for (int i=0; i<=N; i++) {
    float difference = fabsf(out[t][i] - out_check[t][i]);
    if (difference > 0.00001)
      printf("%d,%d --> %f,%f\n", t, i, out[t][i], out_check[t][i]);
  }
  #endif 

}
