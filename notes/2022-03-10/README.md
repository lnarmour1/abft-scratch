## 2022/03/09

### News

We can generate low overhead code that computes the stencil kernel by doing two separate ISL codegens:

1. one for the abft checksum tile parameterized by T, I, W, S, and t (cT, cI, cW, cS, and ct respectively below). This the [same thing we've discussed before](jac1d1r.ipynb).
2. one for the stencil kernel fused with the fixed-sized checksum tile origins.

At each time step, we first compute the entire stencil and then process the abft region points by calling into the code from generated from 1.
This leaves much prettier code that makes it easier for the compiler to deliver good performance.
See [S.20.W.800.c](S.20.W.800.c) for working example.

Parametric ABFT tile codegen (i.e., defined [here](S.20.W.800.iscc#L94)):
```
{
  if (cT + cI >= ct + 2 && cT >= ct + 1 && cS + ct >= cT && ct >= 0 && T >= ct && N + ct + 2 >= cT + cI && cW + 2 * ct + 2 >= 2 * cT)
    R1(ct, cT + cI - ct - 2, cT - ct);
  if (cT >= ct + 1 && cT + cI >= ct + 1 && cS + ct >= cT && ct >= 0 && T >= ct && N + ct + 1 >= cT + cI && cW + 2 * ct + 2 >= 2 * cT) {
    B1(ct, cT + cI - ct - 1, cT - ct);
  } else if (cT >= 0 && T >= cT && cS >= 1 && ct == cT) {
    for (int c1 = max(0, cI); c1 <= min(N, cI + cW); c1 += 1)
      C(cT, c1, 0);
  }
  if (cS >= 1 && cT >= cS && T + cS >= cT && cS + ct == cT)
    for (int c1 = max(0, cI + cS); c1 <= min(N, cI + cW - cS); c1 += 1)
      K(cT - cS, c1, cS);
  if (cT >= ct + 1 && N + cT >= cI + cW + ct + 1 && cS + ct >= cT && ct >= 0 && T >= ct && cI + cW + ct + 1 >= cT && cW + 2 * ct + 2 >= 2 * cT)
    B2(ct, -cT + cI + cW + ct + 1, cT - ct);
  if (N + cT >= cI + cW + ct + 2 && cT >= ct + 1 && cS + ct >= cT && ct >= 0 && T >= ct && cI + cW + ct + 2 >= cT && cW + 2 * ct + 2 >= 2 * cT)
    R2(ct, -cT + cI + cW + ct + 2, cT - ct);
}
```

Stencil + fixed-size ABFT tile origins (with S=20 and W=800) codegen (i.e., defined [here](S.20.W.800.iscc#L95)):
```
{
  for (int c3 = 0; c3 <= N; c3 += 1)
    S0(0, c3);
  if (N <= -1) {
    for (int c1 = 1; c1 <= T; c1 += 1) {
      S3(c1, N);
      S1(c1, 0);
    }
  } else {
    for (int c1 = 0; c1 <= T; c1 += 1) {
      if (c1 >= 1) {
        S1(c1, 0);
        for (int c3 = 1; c3 < N; c3 += 1)
          S2(c1, c3);
        S3(c1, N);
      }
      for (int c3 = 0; c3 <= floord(N, 760); c3 += 1)
        ABFT(c1, 760 * c3, c1 / 21, c3, -(c1 % 21) + c1 + 20);
    }
  }
}
```

### Experiments

As the ABFT tile size increases, the overhead decreases as expected.

![overhead](overhead-w-S.png)

Randomly flipping a single bit at one point during the execution and measuring whether or not we can observe a difference between the stencil and ABFT greater than 0.0001.
But I chose 0.0001 only after manually running a few times for this problem size and for the initial values, in [0,10).
How should we choose this threshold generally?

![error](error-w-S.png)

Data for these plots come from this repo @85d03b174b2d6eeca68e428296ea65e7efa6e5ab
* [expm/6.200](expm/6.200)
* [expm/6.400](expm/6.400)
* [expm/6.800](expm/6.800)

### Outstanding issues

1. How to choose the threshold to decide whether the discrepancy between the stencil kernel and the ABFT is due to floating point rounding or an actual error.
2. The code that iterates over checksum tiles requires a polynomial schedule in order to get full coverage since the first and last timesteps of time-adjacent tiles must be overlapped. The stencil+abft tile origin code above does not reflect this at the moment, but it probably should.
3. How to generalize this for any 1D stencil.
4. The boundary ABFT tiles need to be handled differently, and are currently being ignored.
