from islpy import *
from islplot.support import *

def walk_points(bset, reversed=None):
    points = bset_get_points(bset)
    if reversed:
        points.reverse()
    for point in points:
        print(point)


def main():
    d = Set('[T,I,W]->{[t,i] : 0<=t,i<=100 }')

    pre = Map('[T,I,W]->{ [t,i]->[0,t,i]}')
    post = Map('[T,I,W]->{ [_,t,i]->[t,i]}')

    M = Map('''[T,I,W]->{ 
                        [0,t,i]->[0,t-1,i] : 0<(i-I)+(t-T) and (i-I)-(t-T)<W;
                        [0,t,i]->[1,t-1,i] : 0=(i-I)+(t-T) or (i-I)-(t-T)=W;
                        [0,t,i]->[2,t-1,i-1] : 0=(i-I)+(t-T);
                        [0,t,i]->[2,t-1,i+1] : (i-I)-(t-T)=W;
                        [1,t,i]->[1,t,i] ;
                        [2,t,i]->[2,t,i] ;
            }''')
    Mp, exact = M.power()
    print(exact)

    D = 4
    T = 4
    I = 2
    W = 13

    dom = BasicSet('[T={},I={},W={}]->{{[T,i]}}'.format(T, I, W))

    _D = BasicSet('[T,I,W]->{{[{}]}}'.format(D))
    bset = d.intersect(dom).apply(pre).apply(_D.apply(Mp).unwrap())




if __name__ == '__main__':
    #power_ex()
    main()